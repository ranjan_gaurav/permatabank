<?php if (@$error): ?>
    <div class="alert">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <?php echo $error; ?>
    </div>
<?php endif; ?>
<?php if ($this->session->flashdata('message')) { ?>
    <div class="alert alert-success">
        <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
    </div>
<?php } ?>

<div class="passwordBox animated fadeInDown">
    <div class="row">

        <div class="col-md-12">
            <div class="ibox-content">

                <h2 class="font-bold">Forgot password</h2>

                <p>
                    Enter your email address and your password will be reset and emailed to you.
                </p>

                <div class="row">

                    <div class="col-lg-12">
                        <form class="m-t" role="form" method="post">
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email address" required="" name="user_email" value="<?php echo set_value('user_email'); ?>">
                            </div>

                            <button type="submit" class="btn btn-primary block full-width m-b">Send new password</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr/>
</div>

