<section>
    <div class="section-body contain-lg">
        <div class="row">

            <!-- BEGIN ADD CONTACTS FORM -->
            <div class="col-md-12">
                <div class="card">
                    <?php if (@$error) { ?>
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Danger ! </strong> <?php echo $error; ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('message')) { ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Success ! </strong> <?php echo $this->session->flashdata('message'); ?>
                        </div>
                    <?php } ?>
                    <div class="card-head style-primary">
                        <header>Change Password</header>
                    </div>
                    <form class="form" role="form form-validate" method="post" action="" id="userForm" >

                        <!-- BEGIN DEFAULT FORM ITEMS -->

                        <div class="card-body style-primary form-inverse">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group floating-label">
                                        <input type="password" class="form-control" id="cur_password" value="<?php echo set_value('cur_password'); ?>" name="cur_password" required>
                                        <label for="cur_password">Current Password</label>
                                    </div>
                                </div><!--end .col -->
                                <div class="col-xs-12">
                                    <div class="form-group floating-label">
                                        <input type="password" class="form-control" id="new_password" value="<?php echo set_value('new_password'); ?>" name="new_password" required>
                                        <label for="new_password">New Password</label>
                                    </div>
                                </div><!--end .col -->
                                <div class="col-xs-12">
                                    <div class="form-group floating-label">
                                        <input type="password" class="form-control" id="cnf_password" value="<?php echo set_value('cnf_password'); ?>" name="cnf_password" required>
                                        <label for="cnf_password">Confirm New Password</label>
                                    </div>
                                </div><!--end .col -->
                            </div><!--end .row -->
                        </div><!--end .card-body -->
                        <!-- END DEFAULT FORM ITEMS -->

                        <!-- BEGIN FORM FOOTER -->
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <button type="submit" class="btn btn-flat btn-primary ink-reaction">Change Password</button>
                            </div><!--end .card-actionbar-row -->
                        </div><!--end .card-actionbar -->
                        <!-- END FORM FOOTER -->

                    </form>
                </div><!--end .card -->
            </div><!--end .col -->
            <!-- END ADD CONTACTS FORM -->

        </div><!--end .row -->
    </div><!--end .section-body -->
</section>