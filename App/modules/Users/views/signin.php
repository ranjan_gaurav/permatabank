<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div class="logo-img">
		<img alt="logo" src="<?php echo base_url(); ?>/assets/img/logo-permata-bank.png" />
        </div>
        <form class="m-t" role="form" method="post">
            <?php if (@$error): ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">�</button>
                    <?php echo $error; ?>
                </div>
            <?php endif; ?>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="Username" required="" id="email" name="email">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Password" required="" id="password" name="password">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
			<div class="remember_me">
			<div class="remember_me-le"><input type="checkbox" tabindex="3"> Remember Me</div>
            <div class="remember_me-ri"><a href="<?php echo base_url(); ?>users/forgot_password"><small>Forgot password?</small></a></div>
			</div>
           <!-- <p class="text-muted text-center"><small>Do not have an account?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="#">Create an account</a> -->
        </form>
    </div>
</div>