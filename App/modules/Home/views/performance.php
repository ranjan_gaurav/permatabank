<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>Team Performance Overview</h2></div>
</div>
 
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12 col-sm-12 p-d-b">
            <div class="col-lg-6 col-sm-9 col-md-8 padding-l">
                <!--- date picker--->
                <div class="col-lg-6 col-sm-6 col-md-6 padding-l">
                    <div class="form-group" id="data_3">
                        <label class="font-noraml">Start Date</label>
                        <div class="form-group" id="performance_decade_view1">
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input class="form-control" value="03/04/2014" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6 padding-l">
                    <div class="form-group" id="data_3">
                        <label class="font-noraml">End Date</label>
                        <div class="form-group" id="performance_decade_view2">
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input class="form-control" value="03/04/2014" type="text">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!--             <div class="col-lg-6 col-sm-3 col-md-4 padding-r"> -->
 <!--                <div class="top-btn-sec"><a href="<?php //echo base_url('home'); ?>" class="btn btn-w-m btn-info">Summary</a></div> -->
<!--             </div> -->
        </div>
        <div class="col-lg-9 col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content hun-performance">
                    <div class="content demo-yx" id="">
                        <div class="table-responsive5 double">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>KPI</th>
                                        <th class="text-center1" colspan="8">Process Indicator</th>
                                    </tr>
                                    <tr>
                                        <th rowspan="" valign="middle"><span class="text-roted">Hunter Name</span> </th>
                                        <th><span class="text-roted">Loan NTB (MTD)</span></th>
                                        <th><span class="text-roted">No of Leads </br>Created (MTD)</span></th>
                                        <th><span class="text-roted">Visits</span></th>
                                        <th><span class="text-roted">No. Of Appli. </br>accepted </br>by CF (MTD)</span></th>
                                        <th><span class="text-roted">No. Of Appli. </br>approved (MTD)</span></th>
                                        <th><span class="text-roted">Conversion rate </br>(Last 30 Days)</span></th>
                                        <th><span class="text-roted">Avg. Visit per lead</span></th>
                                        <th><span class="text-roted">TAT interested to </br>accepted by CF </br>(Last 30 Days)</span></th>
                                        <th><span class="text-roted">TAT accepted </br>by CF to</br> disbursement </br>(Last 30 Days)</span></th>

                                    </tr>

                                </thead>
                                <tbody>
                                    <?php  foreach ($result as $list) : 
                                    $name = $list->hunter_name; ?>
                                        
                                       
                                       <tr class="gradeX">
                                           <td >
                                           
                                           <a data-toggle="modal" class="withdraw" id="<?php echo $list->userid; ?>" data-id="<?php echo $list->hunter_name; ?>" onclick="changeProfileInformation(this.id);" ><?php echo $name; ?></a>
                                           
                                           </td>
                                            <td><?php echo $list->sum_ntb; ?></td>
                                            <td><?php echo $list->sum_lead_creation; ?></td>
                                            <td><?php echo $list->sum_visits; ?></td>
                                            <td><?php echo $list->sum_approx_approval_rate; ?></td>
                                            <td><?php echo $list->sum_successful_refererrals; ?></td>
                                            <td><?php echo $list->sum_lead_conversion .'%'; ?></td>
                                            <td><?php echo $list->visit_per_lead; ?></td>
                                            <td><?php echo $list->sum_tat_bt_sub_to_onboard; ?></td>
                                            <td><?php echo $list->tat_bt_created_to_sub; ?></td>
                                           
                                        </tr>
                                                                              
                                    <?php endforeach; ?>

                                    <tr class="gradeX">
                                        <td>Overall team</td>
                                        <td>12</td>
                                        <td>31</td>
                                        <td>125</td>
                                        <td>24</td>
                                        <td>13</td>
                                        <td>10%</td>
                                        <td>16</td>
                                        <td>11</td>
                                        <td>5</td>
                                    </tr>

<!--                                     <tr class="gradeX"> -->
<!--                                         <td>Team Last Month</td> -->
<!--                                         <td>33</td> -->
<!--                                         <td>311</td> -->
<!--                                         <td>398</td> -->
<!--                                         <td>121</td> -->
<!--                                         <td>109</td> -->
<!--                                         <td>69%</td> -->
<!--                                         <td>10.32</td> -->
<!--                                         <td>10.49</td> -->
<!--                                     </tr> -->

                                </tbody>



                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-3 col-sm-12">
            <div class="ibox float-e-margins">

                <div class="ibox-content rigth-see">
                    <div class="content demo-yx" id="">
                        <div class="table-responsive8 double">
                            <table class="table table-striped table-bordered table-hover dataTables">
                                <thead>
                                    <tr>
                                        <th class="text-center1" colspan="4">Alerts</th>

                                    </tr>
                                    <tr>
                                        <th><span class="text-roted">Type</span></th>
                                        <th><span class="text-roted">Description</span></th>
                                        <th><span class="text-roted">Escalated to</span></th>
                                        <th><span class="text-roted">Delete</span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($alerts as $alert) : ?>
                                        <tr class="gradeX">
                                            <td><?php echo $alert->type; ?></td>
                                            <td><?php echo $alert->description; ?></td>
                                            <td><?php echo $alert->escalated_to; ?></td>
                                            <td class="center">
                                                <a name="5" class="" id="5" href="javascript:void(0);">
                                                    <i class="fa fa-times"></i>
                                                </a>

                                            </td>
                                        </tr>
                                        
                                         
                                    <?php endforeach; ?>


                                </tbody>

                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="clearfix"></div>
</div>





<div id="myModal" class="modal">
    <div id="modal-content" class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">
            <input type="hidden" id="P_reqesturl" value="<?php echo base_url('home/users_info'); ?>" /> 
            <?php
            $this->load->view('modalperformance');
            ?>
        </div>
    </div>
</div>




</script>
   