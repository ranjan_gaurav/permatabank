<div class="profile-image">
    <img src="<?php echo base_url(); ?>/assets/img/<?php echo ($users->user_pic)?$users->user_pic:'a4.jpg';?>" class="img-circle circle-border m-b-md" alt="profile">
</div>
<div class="profile-info">
    <div class="">
        <div>
            <h2 class="no-margins">
                <?php echo ($users->first_name)?$users->first_name:'WILLIAM WINATA';?>
            </h2>
            <h4>+62 2345 2345</h4>
            <h4><?php echo strtolower(($users->username)?$users->username:'willliam');?>@btn.co.id</h4>
        </div>
    </div>
</div>