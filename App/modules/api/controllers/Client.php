<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.in>
 * Date: 08/06/2016
 * Time: 01:28 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Client extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('client_model','client');
        $this->load->helper('string');
    }

    /**
     * @method : list Users using GET Method
     * @method description: call to get list of Users.
     * @param : emp_id
     * @data: Users Data
     */
    public function list_post() {
        $id = (int) $this->post('client_id');
        log_message('info', 'data=' . $id);
        $clients = $this->client->getAllClients($id);

        if (!empty($clients)) {
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'message' => 'Success',
                'data' => $clients
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->set_response([
                'status' => FALSE,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK);
        }
    }
    
    /**
     * @method : listing app_Users using GET Method
     * @method description: call to get list of Users.
     * @param : emp_id
     * @data: Users Data
     */
    public function afterid_post() {
        $id = (int) $this->post('client_id');
        log_message('info', 'data=' . $id);
        $clients = $this->client->getAllClientsAfterId($id);

        if (!empty($clients)) {
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'message' => 'Success',
                'data' => $clients
                    ], REST_Controller::HTTP_OK);
        } else {
            $this->set_response([
                'status' => FALSE,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK);
        }
    }
    
    
    
    
    
}
