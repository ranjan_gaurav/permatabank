<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>Pipeline Summary</h2></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">            
            <div class="ibox float-e-margins">
                <div class="ibox-content content-s">
                    <div class="table-responsive">
        <!-- <table class="table text-center">
            <thead>
                <tr>
                    <th class="yellow-bg rightarrow1"><p class="text-center">Lending lead <br>by status</p></th>
            <th class="red-bg rightarrow"><p class="text-center">(0)<br>Lead Allocated</p></th>
            <th class="red-bg rightarrow"><p class="text-center">(1)<br>Lead Unallocated</p></th>
            <th class="red-bg rightarrow"><p class="text-center">(2)<br>Client engaged</p></th>
            <th class="red-bg rightarrow"><p class="text-center">(3)<br>App. ongoing</p></th>
            <th class="red-bg rightarrow"><p class="text-center">(4)<br>Client approved</p></th>
            <th class="red-bg rightarrow2"><p class="text-center">(4)<br>Client On-board</p></th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="v-middle">                                        
                        <h3>Today <br>Snapshot</h3>
                        <div class="row vertical-align">
                            <div class="col-xs-12">
                                <p class="font-noraml1"><i class="fa fa-square fa-1x om"></i> &nbsp; Target</p>
                            </div>
                        </div>
                        <div class="row vertical-align">
                            <div class="col-xs-12">
                                <p class="font-noraml1"><i class="fa fa-square fa-1x dm"></i> &nbsp; Achievement</p>
                            </div>                                            
                        </div>
                    </td>
                    <td>
                        <div id="SMEchart8"></div> 
                        <div id="SMEchart9"></div> 									
                    </td>
                    <td>
                        <div id="SMEchart10"></div> 	
                    </td>
                    <td>
                        <div class="bar-chart3">
                            <div id="SMEchart11"></div> 
                        </div>
                    </td>
                    <td>
                        <div class="bar-chart3">
                            <div id="SMEchart12"></div> 
                        </div>
                    </td>
                    <td>
                        <div class="bar-chart3">
                            <div id="SMEchart13"></div> 
                        </div>
                    </td>
                    <td>
                        <div id="SMEchart14"></div>
                        <div id="SMEchart15"></div> 								 
                    </td>
                </tr>
                <tr>
                    <td><p>Value*</p></td>
                    <td> </td>
                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">9 Mn</span></p></td>
                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">6 Mn</span></p></td>
                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">3 Mn</span></p></td>
                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">3 Mn</span></p></td>
                    <td><p class="text-center"><span class="label label-danger font-size-14 circle50">2 Mn</span></p></td>
                </tr>  
                <tr>
                    <td colspan="7">
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-4 col-sm-6 col-md-6">
                                <div class="form-group" id="data_3">
                                    <label class="font-noraml">Start Date</label>
                                    <div class="form-group" id="data_decade_view1">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="03/04/2014">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-6 col-md-6">
                                <div class="form-group" id="data_3-1">
                                    <label class="font-noraml">End Date</label>
                                    <div class="form-group" id="data_decade_view2">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="03/04/2014">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2"></div>
                        </div>
                    </td>
                </tr> 
                <tr>
                    <td><p>Win Rate</p></td>                                        
                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">100%</span></p></td>
                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">100%</span></p></td>
                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">33%</span></p></td>
                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">20%</span></p></td>
                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">16%</span></p></td>
                    <td><p class="text-center"><span class="label label-danger font-size-14 circle50">14%</span></p></td>
                </tr>
                <tr>
                    <td><p>Avg. TAT</p></td>                                        
                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">xx days</span></p></td>
                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">xx days</span></p></td>
                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">xx days</span></p></td>
                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">xx days</span></p></td>
                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">xx days</span></p></td>
                    <td><p class="text-center"><span class="label label-danger font-size-14 circle50">xx days</span></p></td>
                </tr>
                <tr>
                    <td><p>Avg. Visits</p></td>                                        
                    <td></td>
                    <td></td>
                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">x visits</span></p></td>
                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">x visits</span></p></td>
                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">x visits</span></p></td>
                    <td><p class="text-center"><span class="label label-danger font-size-14 circle50">x visits</span></p></td>
                </tr>
            </tbody>
        </table> -->
                        <table class="table text-center">
                            <thead>
                                <tr>
                                    <th class="yellow-bg rightarrow1"><p class="text-center2">Assigned</p></th>
                            <th class="red-bg rightarrow"><p class="text-center2">Interest</p></th>
                            <th class="red-bg rightarrow"><p class="text-center2">Application<br/>Preparation</p></th>
                            <th class="red-bg rightarrow"><p class="text-center2">Accepted by <br>CF</p></th>
                            <th class="red-bg rightarrow"><p class="text-center2">Appealed</p></th>
                            <th class="red-bg rightarrow2"><p class="text-center2">Approved</p></th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="v-middle">                                        
                                        <h3>Today <br>Snapshot</h3>
                                        <!-- <div class="row vertical-align">
                                            <div class="col-xs-12">
                                                <p class="font-noraml1"><i class="fa fa-square fa-1x om"></i> &nbsp; Target</p>
                                            </div>
                                        </div> 
                                        <div class="row vertical-align">
                                            <div class="col-xs-12">
                                                <p class="font-noraml1"><i class="fa fa-square fa-1x dm"></i> &nbsp; Achievement</p>
                                            </div>                                            
                                        </div>-->
                                    </td>
                                    <td>
                                        <div id="SMEchart8"></div> 
                                        <!-- <div id="SMEchart9"></div>--> 									
                                    </td>
                                    <td>
                                        <div id="SMEchart10"></div> 	
                                    </td>
                                    <td>
                                        <div class="bar-chart3">
                                            <div id="SMEchart11"></div> 
                                        </div>
                                    </td>
                                    <td>
                                        <div class="bar-chart3">
                                            <div id="SMEchart12"></div> 
                                        </div>
                                    </td>
                                    <td>
                                        <div class="bar-chart3">
                                            <div id="SMEchart13"></div> 
                                        </div>
                                    </td>
                                    <!--<td>
                                        <div id="SMEchart14"></div>
                                         <div id="SMEchart15"></div> 								 
                                    </td>
                                    <td>
                                        <div id="SMEchart15"></div>

                                    </td>--> 
                                </tr>
                                <tr>
                                    <td><p>Distribution</p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">26%</span></p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">26%</span></p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">13%</span></p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">10%</span></p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">10%</span></p></td>
                                </tr> 
                                <tr>
                                    <td><p>Value*</p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">16 bn</span></p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">16 bn</span></p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">8 bn</span></p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">2 bn</span></p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">6 bn</span></p></td>
                                </tr>
								<tr>
                                    <td colspan="2"><p></p></td>
                                    <td><p class="text-center"><b>Rejected</b></p></td>
                                    <td><p class="text-center"><b>Withdrawn</b></p></td>
                                    
                                    <td colspan="2"><p class="text-center"><b>Loan Disbursement</b></p></td>
                                    
                                </tr>
								<tr>
                                    <td colspan="2"><p>Today</p></td>
                                    <td><p class="text-center">1</p></td>
                                    <td><p class="text-center">0</p></td>
                                    
                                    <td colspan="2"><p class="text-center">1</p></td>
                                    
                                </tr> 
								<tr>
                                    <td colspan="2"><p>Month to Date</p></td>
                                    <td><p class="text-center">2</p></td>
                                    <td><p class="text-center">0</p></td>
                                    <td colspan="2"><p class="text-center">2</p></td>
                                    
                                </tr> 								
                                <tr>
								<td class="bg-cl-tr">&nbsp;</td>
                                    <td colspan="4" class="bg-cl-tr">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="form-group" id="data_3">
                                                    <label class="font-noraml">Start Date</label>
                                                    <div class="form-group" id="data_decade_view1">
                                                        <div class="input-group date">
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="03/04/2014">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="form-group" id="data_3-1">
                                                    <label class="font-noraml">End Date</label>
                                                    <div class="form-group" id="data_decade_view2">
                                                        <div class="input-group date">
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="03/04/2014">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </td>
								<td class="bg-cl-tr">&nbsp;</td>
                                </tr> 
                                <tr>
                                    <td><p>Win Rate</p></td>                                        
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">100%</span></p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">100%</span></p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">33%</span></p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">20%</span></p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">16%</span></p></td>

                                </tr>
                                <tr>
                                    <td><p>Avg. TAT</p></td>                                        
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">5</span></p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">5</span></p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">4</span></p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">5</span></p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">3</span></p></td>

                                </tr>
                                <tr>
                                    <td><p>Avg. Visits</p></td>                                        
                                    <td></td>
                                    <td></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">3</span></p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">1.5</span></p></td>
                                    <td><p class="text-center"><span class="label label-warning font-size-14 circle50">1.5</span></p></td>

                                </tr>
                            </tbody>
                        </table>
                    </div>                                
                </div>
            </div>            
        </div>
    </div>
    <div class="row">

        <div class="col-lg-6 col-sm-6 col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lead Distribution by Industry</h5>
                </div>
                <div class="ibox-content">
                    <div class="chart-pei">
                        <div id="chartContainer26"></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lead Distribution by Interested </h5>
                </div>
                <div class="ibox-content">
                    <div class="chart-pei">
                        <div id="chartContainer27"></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="col-lg-12"> 
            <div class="lead-details-btns text-right">
                <button class="btn btn-w-m btn-info" type="button">Go to Back</button>
            </div>
        </div> -->
    </div>
</div>