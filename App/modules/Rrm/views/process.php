<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>RRO Performance Dashboard</h2></div>
</div>
<?php
$hunterInfo = getUserInfo(@$this->session->userdata('userid'));
?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-8 col-md-6 col-sm-6">
            <!--<div class="view-btn">
                <div class="form-group">
                    <div class="view-name">View :<span><?php echo $hunterInfo->first_name; ?></span></div>
                </div>
            </div> -->
            <div class="profile-image">
                <img src="<?php echo base_url(); ?>/assets/img/<?php echo $hunterInfo->user_pic; ?>" class="img-circle circle-border m-b-md" alt="profile">
            </div>
            <div class="profile-info">
                <div class="">
                    <div>
                        <h2 class="no-margins">
                            <?php echo $hunterInfo->first_name; ?>
                        </h2>
                        <h4>+62 2345 2345</h4>
                        <h4><?php echo $hunterInfo->username; ?>@btn.co.id</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">

        </div>

    </div>
    <?php
    $array = array();
    foreach ($result as $arrdata) :
        array_push($array, $arrdata->ntb);
    endforeach;
    ?>
    <script>
        var financialntb = [];
        var ntbwithloadbundle = [];
        var ntbwithloan = [];
        var ntbwithoutloan = [];
        var successfullrefferals = [];
        var months = [];
<?php foreach ($result as $data) : ?>
            financialntb.push(['<?php echo $data->ntb ?>']);
            ntbwithloadbundle.push(['<?php echo $data->ntb_with_bundle ?>']);
            ntbwithloan.push(['<?php echo $data->ntb_with_loan_only ?>']);
            ntbwithoutloan.push(['<?php echo $data->ntb_without_loan ?>']);
            successfullrefferals.push(['<?php echo $data->successful_refererrals ?>']);
            months.push(['<?php echo date('F', strtotime($data->creation_date)) ?>']);
<?php endforeach; ?>
    </script>
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content rro-performance">
                    <div class="content demo-yx" id="">
                        <div class="table-responsive5 double">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="bg-g text-center1" rowspan="2">Month-To-Date (MTD)</th>
                                        <th colspan="2" class="text-center1">Up-sell / Cross-sell</th>
                                        <th colspan="2" class="text-center1">Payment Reminder </th>
                                        <th colspan="2" class="text-center1">Servicing and Relationship Calls</th>
                                        <th colspan="2" class="text-center1">Total</th>
                                    </tr>
                                    <tr>

                                        <th class="text-center1">No</th>
                                        <th class="text-center1">%</th>
                                        <th class="text-center1">No</th>
                                        <th class="text-center1">%</th>
                                        <th class="text-center1">No</th>
                                        <th class="text-center1">%</th>
                                        <th class="text-center1">No</th>
                                        <th class="text-center1">%</th>
                                    </tr>

                                </thead>
                                <tbody>
                                    <tr class="gradeX">
                                        <td>Tasks assigned this month</td>
                                        <td class="text-center1">5</td>
                                        <td class="text-center1">22%</td>
                                        <td class="text-center1">2</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">3</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">10</td>
                                        <td class="text-center1">25%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>Tasks overdue</td>
                                        <td class="text-center1">4</td>

                                        <td class="text-center1">22%</td>
                                        <td class="text-center1">2</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">3</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">9</td>
                                        <td class="text-center1">23%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>Newly Assigned</td>
                                        <td class="text-center1">5</td>

                                        <td class="text-center1">22%</td>
                                        <td class="text-center1">2</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">3</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">10</td>
                                        <td class="text-center1">25%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>Tasks created by RRO (MTD)</td>
                                        <td class="text-center1">6</td>

                                        <td class="text-center1">33%</td>
                                        <td class="text-center1">3</td>
                                        <td class="text-center1">18%</td>
                                        <td class="text-center1">2</td>
                                        <td class="text-center1">18%</td>
                                        <td class="text-center1">11</td>
                                        <td class="text-center1">28%</td>
                                    </tr>
                                    <tr class="gradeD">
                                        <td>Total tasks for current month</td>
                                        <td class="text-center1">20</td>

                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">9</td>
                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">11</td>
                                        <td class="text-center1">100%</td>
                                        <td class="text-center1">40</td>
                                        <td class="text-center1">100%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>% Successful</td>
                                        <td class="text-center1">2</td>
                                        <td class="text-center1">33%</td>
                                        <td class="text-center1">3</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">3</td>
                                        <td class="text-center1">27%</td>
                                        <td class="text-center1">8</td>
                                        <td class="text-center1">20%</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>% Closed (Successful + Failed)</td>
                                        <td class="text-center1">4</td>

                                        <td class="text-center1">56%</td>
                                        <td class="text-center1">5</td>
                                        <td class="text-center1">45%</td>
                                        <td class="text-center1">5</td>
                                        <td class="text-center1">45%</td>
                                        <td class="text-center1">14</td>
                                        <td class="text-center1">35%</td>
                                    </tr>
                                    <tr class="gradez">
                                        <td>Total no. of activities</td>
                                        <td class="text-center1" colspan="2">10</td>
                                        <td class="text-center1" colspan="2">8</td>
                                        <td class="text-center1" colspan="2">10</td>
                                        <td class="text-center1" colspan="2">36</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-sm-12 col-md-12">
            <div class="row">
                <h4 class="heading-tip">Week-To-Date (WTD)</h4>
                <div class="col-lg-6 col-sm-6 col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title"><h5>Up-sell / Cross-sell</h5></div>
                        <div class="ibox-content">
                            <div class="rrochart">
                                <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/rro/graph_new_01.svg" class="img-t" alt="Up-sell-Cross-sell-r" height="320">
                            </div>
                            <div class="del-am">
                                <span>No. Activities Today</span>
                                <small>No. Activities WTD</small>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title"><h5>Loan Maintenance</h5></div>
                        <div class="ibox-content">
                            <div class="rrochart">
                                <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/rro/graph_new_02.svg" class="img-t" alt="Loan Maintenance" height="320">
                            </div>
                            <div class="del-am">
                                <span>No. Activities Today</span>
                                <small>No. Activities WTD</small>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title"><h5>Relationship Calls</h5></div>
                        <div class="ibox-content">
                            <div class="rrochart">
                                <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/rro/graph_new_03.svg" class="img-t" alt="Relationship Calls" height="320">
                            </div>
                            <div class="del-am">
                                <span>No. Activities Today</span>
                                <small>No. Activities WTD</small>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title"><h5>Servicing and Relationship Calls</h5></div>
                        <div class="ibox-content">
                            <div class="rrochart">
                                <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/rro/graph_new_04.svg" class="img-t" alt="Relationship Calls" height="320">
                            </div>
                            <div class="del-am">
                                <span>No. Activities Today</span>
                                <small>No. Activities WTD</small>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

