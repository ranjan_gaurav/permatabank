<table class="table table-striped table-bordered table-hover dataTables-example" >
    <thead>
        <tr>
            <th></th>
            <th>Name</th>
            <th>Area Code</th>
            <th>Capacity</th>
            <th>Performance Ranking</th>
            
        </tr>
    </thead>
    <tbody>
        <?php foreach ($sales as $users) : ?>
            <tr class="gradeX">
                <td><input name="sales_id" type="radio" value='<?php echo $users->staff_id; ?>'></td>
                <td><?php echo $users->name; ?></td>
                <td><?php echo $users->area; ?></td>
                <td><?php echo $users->capacity; ?></td>
                <td><?php echo $users->product; ?></td>
               
            </tr>
        <?php endforeach; ?>
    </tbody>

</table>