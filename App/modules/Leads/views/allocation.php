<?php
/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 5/4/2016
 * Time: 6:56 PM
 */
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>Lead Allocator</h2></div>
</div>
<?php if (@$error): ?>
    <div class="alert">
        <button type="button" class="close" data-dismiss="alert">�</button>
        <?php echo $error; ?>
    </div>
<?php endif; ?>

<?php if ($this->session->flashdata('message')) { ?>
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
    </div>
<?php } ?>
<div class="row">
    <div class="col-lg-12 data-table-content">
        <div class="ibox float-e-margins">
            <div class="select-box-area">
                <div class="col-lg-4 col-lg-4 col-lg-4">
                    <label class="filter">Filter-Area</label>
                    <div class="filter-area">
                        <select class="selectpicker" id='area_select' name='area' onchange='SearchAreaResult();' >
                            <option value=''>Select Area</option>
                            <option value='S&D-001'>S&D-001</option>
                            <option value='S&D-002'>S&D-002</option>
                            <option value='S&D-003'>S&D-003</option>
                            <option value='S&D-004'>S&D-004</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4 col-lg-4 col-lg-4" style="display:none;">
                    <label class="filter">Filter-Industry</label>
                    <div class="filter-area">
                        <select class="selectpicker" name="product_interest" id="indus_id" onchange='SearchIndustryResult();' >
                            <option value="">Select Industry</option>
                            <option value="OD Facility">OD Facility</option>
                            <option value="Term Loan">Term Loan</option>
                            <option value="OD Bundle">OD Bundle</option>
                            <option value="Term Loan Bundle">Term Loan Bundle</option>
                            <option value="CASA">CASA</option>
                            <option value="CASA Bundle">CASA Bundle</option>
                            <option value="TD">TD</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-4 col-lg-4 col-lg-4" style="display:none;">
                    <label class="filter">Filter-Sector</label>
                    <div class="filter-area">
                        <select class="selectpicker" name="industry_id" id="product_interest" onchange='SearchSectorResult();'>
                            <option value="">Select Sector</option>
                            <option value="Retail">Retail</option>
                            <option value="Tourism">Tourism</option>
                            <option value="Autoparts">Autoparts</option>
                            <option value="Real Estate">Real Estate</option>
                            <option value="Restaurants">Restaurants</option>
                            <option value="Education">Education</option>
                            <option value="Manufacturing">Manufacturing</option>
                            <option value="F&B">F&B</option>
                        </select>
                    </div>
                </div>
            </div>
            <form method='post' id='allocate-leads'>
                <div class="col-lg-6 col-sm-6 col-md-6 padding-off-left">
                    <div class="ibox float-e-margins">
                        <div class="grap-content">
                            <div class="ibox-title2"><h5>Unallocated Leads</h5></div>
                            <div class="ibox-content table-contnet-sec">
                                <div id="content-md" class="content">
                                    <div class="table-responsive leads-listing">
                                        <div id='leads-listing'>
                                            <?php $this->load->view('list_leads'); ?>
                                        </div>
                                    </div>
                                    <input type="hidden" id="P_reqesturl" value="<?php echo base_url('leads/listLeads'); ?>" />

                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6 padding-off-right">
                    <div class="ibox float-e-margins">
                        <div class="grap-content">
                            <div class="ibox-title2"><h5>Sales Staff</h5></div>
                            <div class="ibox-content table-contnet-sec">
                                <div id="content-md1" class="content">
                                    <div class="table-responsive">
                                        <div class="table-responsive leads-listing">
                                            <div id='sales-listing'>
                                                <?php $this->load->view('list_sales'); ?>
                                            </div>
                                            <input type="hidden" id="P_reqesturl_sales" value="<?php echo base_url('leads/listSales'); ?>" />
                                        </div>

                                    </div>
                                </div>
                                
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="lead-btns">
				<button class="btn btn-w-m btn-primary btns-q" type="submit">Refer Leads</button>
				<button class="btn btn-w-m btn-warning btns-q" type="submit">Allocate Lead</button>
				</div>
            </form>
            <div class="clearfix"></div>

        </div>
    </div>
</div>
