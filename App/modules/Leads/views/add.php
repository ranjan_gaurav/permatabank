<?php
/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 5/4/2016
 * Time: 6:56 PM
 */
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>Create New Lead</h2></div>
</div>
<?php if (@$error): ?>
    <div class="panel panel-warning">
        <div class="panel-heading">
            <i class="fa fa-warning"></i> Warning Panel
        </div>
        <div class="panel-body">
            <p><?php //echo $error;              ?>Please fill all mandatory fields.</p>
        </div>
    </div>
<?php endif; ?>

<?php if ($this->session->flashdata('message')) { ?>
    <div class="panel panel-success">
        <div class="panel-heading">
            Success Panel
        </div>
        <div class="panel-body">
            <p><?php echo $this->session->flashdata('message'); ?></p>
        </div>
    </div>
<?php } ?>

<form id="leadform" class="wizard-big" method="post">
    <div class="row">
        <div class="lead-forms-area">
            <div class="col-lg-4 col-sm-4">
                <div class="form-group lead-forms">
                    <label>Sales Staff ID</label>
                    <input type="text" class="form-control" placeholder="EM-005" disabled name="sales_staff_id" value="2873">
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="form-group lead-forms">
                    <label>Lead Creation Date:</label>
                    <input type="text" class="form-control" placeholder="04-05-2016" disabled name="creation_date" value="<?php echo date('Y-m-d'); ?>">
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="form-group lead-forms">
                    <label class="red-high">Lead Source*:</label>
                    <select class="selectpicker form-control required" name="lead_source_id">
                        <option value=''>Select</option>
                        <?php /* foreach ($company as $clist): ?>
                          <option value="<?php echo $clist->company_id; ?>"><?php echo $clist->name; ?></option>
                          <?php endforeach; */ ?>
                        <option value='self-created'>Self-Created</option>
                        <option value='mgm'>MGM</option>
                        <option value='internal-referral'>Internal Referrals</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="form-group lead-forms">
                    <label>Lead ID:</label>
                    <?php $counter = 3210; ?>
                    <input type="text" name="lead_id" class="form-control" value= "<?php echo ++$counter; ?>" readonly="readonly" >
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="form-group lead-forms">
                    <label>Segment:</label>
                    <select class="selectpicker form-control required" name="segment_id">
                        <option value=''>Select Segment</option>
                        <option value="SME">SME</option>
                        <option value="Commercial">Commercial</option>
                        <option value="Individual">Individual</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="form-group lead-forms">
                    <label>Lead Status:</label>
                    <select class="selectpicker form-control required" name="status">
                        <option value="">Select Status</option>
                        <option value="1">Assigned</option>
                        <option value="2">Interested</option>
                        <option value="3">Closed</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="lead-forms-section">
            <div class="col-lg-4 col-sm-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Company Profile</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="form-group lead-forms">
                            <label class="red-high">Name*</label>
                            <input type="text" placeholder="Enter Name" class="form-control required" name="name" value="<?php echo set_value('name'); ?>">
                        </div>
                        <div class="form-group lead-forms">
                            <label class="red-high">Industry*</label>
                            <select class="selectpicker form-control required" name="industry_id">
                                <option value="Industry">Industry</option>
                                <option value="Restaurant">Restaurant</option>
                                <option value="F&B">F&B</option>
                                <option value="FMCG">FMCG</option>
                                <option value="Autoparts">Autoparts</option>
                                <option value="Retails">Retails</option>
                                <option value="Tourism">Tourism</option>
                                <option value="Others">Others</option>
                            </select>
                        </div>
                        <div class="form-group lead-forms">
                            <label>Sales (Mn IDR)</label>
                            <input type="text" placeholder="" class="form-control required" name="sales" value="<?php echo set_value('sales'); ?>">
                        </div>
                        <div class="form-group lead-forms">
                            <label>Assets (Mn IDR)</label>
                            <input type="text" placeholder="" class="form-control required" name="assets" value="<?php echo set_value('assets'); ?>">
                        </div>
                        <div class="form-group lead-forms">
                            <label>Indebtedness</label>
                            <input type="text" placeholder="" class="form-control" name="indebtedness" value="<?php echo set_value('indebtedness'); ?>">
                        </div>
                        <div class="form-group lead-forms">
                            <label>Key Person</label>
                            <input type="text" placeholder="" class="form-control" name="key_person" value="<?php echo set_value('key_person'); ?>">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Contact Information</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="form-group lead-forms">
                            <label class="red-high">Address*</label>
                            <input type="text" placeholder="Enter Name" class="form-control required" name="address" value="Kav., Jl. M.H. Thamrin No.15, Gondangdia, Jakarta">
                        </div>
                        <div class="form-group lead-forms">
                            <label>Branch</label>
                            <select class="selectpicker" name="branch">
                                <option value="">Select Branch</option>
                                <option value="Branch 1">Branch 1</option>
                                <option value="Branch 2">Branch 2</option>
                                <option value="Branch 3">Branch 3</option>
                            </select>
                        </div>
                        <div class="form-group lead-forms">
                            <label>Area</label>
                            <select class="selectpicker" name="area">
                                <option value="Area 1">S&D-001</option>
                                <option value="Area 2">S&D-002</option>
                                <option value="Area 3">S&D-003</option>
                                <option value="Area 4">S&D-004</option>

                            </select>
                        </div>
                        <div class="form-group lead-forms">
                            <label>Region</label>
                            <select class="selectpicker" name="region">
                                <option value="Region 1">Region 1</option>
                                <option value="Region 2">Region 2</option>
                                <option value="Region 3">Region 3</option>
                                <option value="Region 4">Region 4</option>
                            </select>
                        </div>

                        <div class="form-group lead-forms">
                            <label class="red-high">Phone*</label>
                            <input type="phone" placeholder="" class="form-control required" name="contact">
                        </div>
                        <div class="form-group lead-forms">
                            <label class="red-high">Email*</label>
                            <input type="email" placeholder="Enter your email" class="form-control required" name="email" value="client@gmail.com">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="ibox float-e-margins produc-int">
                    <div class="ibox-title">
                        <h5>Product Interests</h5>
                        <div class="pull-right">
                            <div class="plus-i addMoreProduct">
                                <i class="fa fa-plus-square-o"></i>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="content" id="slimtest1">
                            <div class="topProductInterest">
                                <div class="productInterest">
                                    <!--<div class="form-group lead-forms">
                                        <label class="">Product Category</label>
                                        <select id = "ddl" class="selectpicker required" name='product1_type' onchange = "configureDropDownLists(this, document.getElementById('ddl2'))">
                                            <option value=''>Select Product</option>
                                            <option value="FundingBundle">Funding Bundle</option>
                                            <option value="FundingOnly">Funding Only</option>
                                            <option value="LendingBundle">Lending Bundle</option>
                                            <option value="LendingOnly">Lending Only</option>
                                            <option value="ValueAddedService">Value Added Service</option>

                                        </select>
                                    </div>-->
                                    <div class="form-group lead-forms">
                                        <label class="">Product Category</label>
                                        <select  class="form-control required" name='product_type[]'>
                                            <option value=''>Select Product</option>
                                            <option value="Funding">Funding</option>
                                            <option value="Lending">Lending</option>

                                        </select>
                                    </div>
                                    <!--<div class="form-group lead-forms lead-forms45">
                            <label>Product 1 Name</label>
                            <div class="dd12">
                                <select id="ddl2" class="" name='product1_name'>
                                    <option value=''>Select Product</option>
                                </select>
                            </div>
                            <a href="<?php //echo base_url('assets/support/sales.pdf');        ?>" target="_blank">Details</a>
                        </div>-->
                                    <div class="form-group lead-forms">
                                        <label class="">Product Interest</label>
                                        <select  class="form-control required" name='product_name[]'>
                                            <option value=''>Product Interest</option>
                                            <option value="term_loan">Term Loan</option>
                                            <option value="term_loan_bundle">Term Loan Bundle</option>
                                            <option value="od_facility">OD Facility</option>
                                            <option value="od_facility_bundle">OD Facility Bundle</option>
                                            <option value="casa">CASA</option>
                                            <option value="casa_bundle">CASA Bundle</option>
                                            <option value="td">TD</option>
                                        </select>
                                        <!--<a href="<?php //echo base_url('assets/support/sales.pdf');        ?>" target="_blank">Details</a>-->
                                    </div>
                                    <div class="form-group lead-forms">
                                        <label class="line-h">Nominal Amount<br/> (Mn IDR)</label>
                                        <input type="text" placeholder="" class="form-control" name='product_amount[]'>
                                    </div>

                                    <div class="form-group lead-forms">
                                        <label class="">Status</label>
                                        <select  class="form-control required" name='status[]'>
                                            <option value=''>Select Status</option>
                                            <option value="Interested">Interested</option>
                                            <option value="Application prep">Application prep</option>
                                            <option value="Accepted by CF">Accepted by CF</option>
                                            <option value="OD Facility Bundle">OD Facility Bundle</option>
                                            <option value="Approved">Approved</option>
                                            <option value="Rejected">Rejected</option>
                                            <option value="Appealed">Appealed</option>
                                            <option value="Withdrawn Pre-Application">Withdrawn Pre-Application</option>
                                            <option value="Withdrawn Post-Application">Withdrawn Post-Application</option>
                                            <option value="Referred">Referred</option>
                                            <option value="Disbursed">Disbursed</option>
                                        </select>
                                    </div>
                                    <?php /* <a href="#" class="btn btn-w-m btn-primary">Go to Report</a>
                                      <a href="#" class="btn btn-w-m btn-warning"  data-toggle="modal" data-target="#myModal4">Historical Report</a> */ ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="lead-details">
            <span>Notes:</span>
            <textarea name="notes" width="100%" class="form-control" cols="20" rows="2"></textarea>
        </div>
        <div class="lead-btns">
            <button type="submit" name = "referleads" value = "saveLeads" class="btn btn-w-m btn-primary">Save</button>
            <button type="submit" name = "referleads" value = "referleads" class="btn btn-w-m btn-warning refer-lead">Refer Lead</button>
           <!-- <a href="<?php //echo base_url('sales/bundle');     ?>" class="btn btn-w-m btn-info">Go to Sale toolkit</a> -->
        </div>
    </div>
    <form>

        <div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated bounceInRight">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title">Report</h4>
                    </div>
                    <div class="modal-body">
                        << Visit 25.5.16 >> Discussed overdraft. Prospect agreed to apply.<br/>
                        << Visit 25.5.16 >> Provided Prospect with information on term loan and overdraft. Prospect asked for time to consider<br/>
                        << Call 25.5.16 >> Prospect interested in lending products. Agreed to follow-up with more information on Danamon options.<br/>
                    </div>
                </div>

            </div>
        </div>
