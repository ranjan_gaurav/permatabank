<?php

/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 5/4/2016
 * Time: 6:56 PM
 */
class Leads_model extends CI_Model {

    var $table = "lead";
    var $reffer = "reffers";
    var $company_table = "company";
    var $users_table = "users";
    var $allocation_table = "lead_allocation";
    var $sales_table = "sales_staff";
    var $leadProductInsert = "lead_product_interest";

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    public function add() {


        /*  $data['segment_id'] = $this->input->post('segment_id');
          $data['lead_source_id'] = $this->input->post('lead_source_id');
          $data */
        $data = $this->input->post();

        array_pop($data);
        $fullPostedData = $data;
        unset($data['product_type']);
        unset($data['product_name']);
        unset($data['product_amount']);
        unset($data['status']);
        unset($data['lead_id']);

        $this->db->set('creation_date', 'NOW()', false);
        $query = $this->db->insert($this->table, $data);
        //echo $this->db->last_query(); die();
        $lead_ids = $this->db->insert_id();
        if ($query) {
            for ($i = 0; $i < count($fullPostedData['product_type']); $i++) {
                $insertData = array('product_type' => $fullPostedData['product_type'][$i],
                    'product_name' => $fullPostedData['product_name'][$i],
                    'product_amount' => $fullPostedData['product_amount'][$i],
                    'status' => $fullPostedData['product_type'][$i],
                );
                $insertData['lead_id'] = $lead_ids;
                $this->db->insert($this->leadProductInsert, $insertData);
                // echo $this->db->last_query(); die();
            }
            return true;
        } else {
            return false;
        }
    }

    public function add_reffrals() {
        $data['lead_id'] = $this->input->post('lead_id');
        $data['lead_name'] = $this->input->post('name');
        $data['branch'] = $this->input->post('branch');
        if ($data['branch'] == '') {
            $data['branch'] = 'Branch 1';
        }
        $data['status'] = $this->input->post('status');
        if ($data['status'] == '') {
            $data['status'] = 'disbursed';
        } else {
            $data['status'] = 'active';
        }
        $data['rm'] = 'Ivan Cristanto';

        $this->db->set('reffral_date', 'NOW()', false);
        $query = $this->db->insert($this->reffer, $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function update($id) {
        $data = $this->input->post();
        array_pop($data);
        $fullPostedData = $data;
        unset($data['product_type']);
        unset($data['product_name']);
        unset($data['product_amount']);
        unset($data['status']);
        unset($data['lead']);
        $this->db->where('lead_id', $id);
        $query = $this->db->update($this->table, $data);
        $lead_id = $id;
        if ($query) {
            for ($i = 0; $i < count($fullPostedData['product_type']); $i++) {
                $insertData = array('product_type' => $fullPostedData['product_type'][$i],
                    'product_name' => $fullPostedData['product_name'][$i],
                    'product_amount' => $fullPostedData['product_amount'][$i],
                    'status' => $fullPostedData['product_type'][$i],
                );
                $insertData['lead_id'] = $lead_id;
                $this->db->insert($this->leadProductInsert, $insertData);
            }
            return true;
        } else {
            return false;
        }
    }

    public function getDirectoryList() {
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getDirectoryNotWithdranList() {
        $this->db->where('withdraw !=', '1');
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getLeadsList() {
        $this->db->where('status', 'unallocated');
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getLeadsSearchList($name, $value) {
        if ($value != '')
            $this->db->where($name, $value);
        $this->db->where('status', 'unallocated');
        $query = $this->db->get($this->table);
        //print_r($query->result());
        //print_r($this->db->last_query());
        return $query->result();
    }

    public function getSalesSearchList($name, $value) {
        if ($value != '')
            $this->db->where($name, $value);
        $query = $this->db->get($this->sales_table);
        //print_r($query->result());
        return $query->result();
    }

    public function getSalesList() {
        $query = $this->db->get($this->sales_table);
        return $query->result();
    }

    public function withdrawLead($id) {
        $data = array(
            'withdraw' => '1'
        );
        $this->db->where('lead_id', $id);
        $query = $this->db->update($this->table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    function updateStatus() {
        $arr_ids = $this->input->post('arr_ids');
        if (!empty($arr_ids)) {
            foreach ($arr_ids as $ids) {
                $data = array(
                    'staff_id' => $this->input->post('sales_id'),
                    'lead_id' => $ids,
                    'status' => 'allocated'
                );
                $insert = $this->db->insert($this->allocation_table, $data);
                if ($insert) {
                    $leadUpdate = array(
                        'status' => 'allocated'
                    );
                    $query = $this->db->where('lead_id', $ids)->update($this->table, $leadUpdate);
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    public function getLeadList() {
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

    public function getLeadInfo($id) {
        $this->db->where('lead_id', $id);
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->row();
    }

    public function getleadID() {
        $this->db->select_max('lead_id');
        $query = $this->db->get('reffers');
        //	print_r($this->db->last_query()); die();
        return $query->row();
    }

}

?>