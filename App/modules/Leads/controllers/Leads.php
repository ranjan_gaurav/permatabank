<?php

/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 5/4/2016
 * Time: 6:56 PM
 */
class Leads extends Front_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library("session");
        $this->load->model('leads_model', 'leads');
    }

    public function index() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/bootbox.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/lead_directory.js'
        );
        $includeCss = array(
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/dataTables/dataTables.responsive.css',
            'assets/css/plugins/dataTables/dataTables.tableTools.min.css',
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css'
        );
        $data ['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data ['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data ['meta_title'] = "Bank BRI Lead Directory";
        $data ['meta_description'] = "Bank BRI";
        $data ['meta_keyword'] = "Bank BRI";
        $data ['result'] = $this->leads->getDirectoryNotWithdranList();
        $this->setData($data);
    }

    function add() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/plugins/staps/jquery.steps.min.js',
            'assets/js/plugins/validate/jquery.validate.min.js',
            'assets/js/bootbox.js',
            'assets/js/leads.js',
            'assets/js/leadsdropdown.js'
        );
        $includeCss = array(
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/steps/jquery.steps.css'
        );
        $data ['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data ['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data ['company'] = $this->leads->getDirectoryList();
        $leadid = $this->leads->getleadID();
        $data['val'] = 1 + ($leadid->lead_id);



        $data ['meta_title'] = "Bank BRI Add Lead";
        $data ['meta_description'] = "Bank BRI";
        $data ['meta_keyword'] = "Bank BRI";
        $this->load->library('form_validation');
        $this->form_validation->set_rules('lead_source_id', 'Lead Source ID', 'trim|required');
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('industry_id', 'Industry', 'trim|required');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        $this->form_validation->set_rules('contact', 'Phone', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        // $this->form_validation->set_rules('product1_type', 'Product Type', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data ['error'] = validation_errors();
        } else {

            $task = $this->input->post('referleads');



            if ($task == 'saveLeads') {

                $insert = $this->leads->add();


                if ($insert) {
                    $this->session->set_flashdata('message', 'Lead has been created successfully');
                    redirect('Leads');
                } else {
                    $data ['error'] = 'Something wrong Happen with database;';
                }
            }

            if ($task == 'referleads') {
                $insert = $this->leads->add_reffrals();
                if ($insert) {
                    $this->session->set_flashdata('message', 'Lead refferals has been created successfully');
                    redirect(base_url('referrals/tracking'));
                } else {
                    $data ['error'] = 'Something wrong Happen with database;';
                }
            }
        }
        $this->setData($data);
    }

    function edit($id = '2') {
        error_reporting(0);
        $id = ($this->input->post('lead')) ? $this->input->post('lead') : $id;
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/plugins/staps/jquery.steps.min.js',
            'assets/js/plugins/validate/jquery.validate.min.js',
            'assets/js/bootbox.js',
            'assets/js/leads.js',
            'assets/js/leadsdropdown.js'
        );
        $includeCss = array(
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/steps/jquery.steps.css'
        );
        $data ['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data ['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data ['company'] = $this->leads->getDirectoryList();
        $leadid = $this->leads->getleadID();
        $data['val'] = 1 + ($leadid->lead_id);
        $data ['meta_title'] = "Bank BRI Edit Lead";
        $data ['meta_description'] = "Bank BRI";
        $data ['meta_keyword'] = "Bank BRI";
        $this->load->library('form_validation');
        $this->form_validation->set_rules('lead_source_id', 'Lead Source ID', 'trim|required');
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('industry_id', 'Industry', 'trim|required');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        $this->form_validation->set_rules('contact', 'Phone', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        // $this->form_validation->set_rules('product1_type', 'Product Type', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data ['error'] = validation_errors();
        } else {
            $task = $this->input->post('referleads');

            if ($task == 'saveLeads') {
                $insert = $this->leads->update($id);
                if ($insert) {
                    $this->session->set_flashdata('message', 'Lead has been Updated successfully');
                    redirect('Leads');
                } else {
                    $data ['error'] = 'Something wrong Happen with database;';
                }
            }

            if ($task == 'referleads') {

                $insert = $this->leads->add_reffrals();

                if ($insert) {
                    $this->session->set_flashdata('message', 'Lead refferals has been created successfully');
                    redirect(base_url('referrals/tracking'));
                } else {
                    $data ['error'] = 'Something wrong Happen with database;';
                }
            }
        }
        $data ['lead'] = $this->leads->getLeadInfo($id);
        $this->setData($data);
    }

    function withdraw() {
        $id = $this->input->post('company_id');
        if (isset($id) and $id != '') {
            $check = $this->leads->withdrawLead($id);
            if ($check) {
                $data ['status'] = true;
                $data ['message'] = "Withdrawn Successfully";
                echo json_encode($data);
            } else {
                $data ['status'] = false;
                $data ['message'] = "Something Went wrong";
                echo json_encode($data);
            }
        } else {
            $data ['status'] = false;
            $data ['message'] = "Something Went wrong";
            echo json_encode($data);
        }
    }

    function allocation() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/staps/jquery.steps.min.js',
            'assets/js/plugins/validate/jquery.validate.min.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/allocation.js'
        );
        $includeCss = array(
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/steps/jquery.steps.css',
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/dataTables/dataTables.responsive.css',
            'assets/css/plugins/dataTables/dataTables.tableTools.min.css'
        );
        $data ['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data ['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data ['meta_title'] = "Bank BRI Add Lead";
        $data ['meta_description'] = "Bank BRI";
        $data ['meta_keyword'] = "Bank BRI";
        if ($this->input->post() != null) {
            $this->form_validation->set_rules('sales_id', 'Sales Staff', 'trim|required');
            if ($this->form_validation->run() == false) {
                $data ['error'] = validation_errors();
            } else {
                $insert = $this->leads->updateStatus();
                if ($insert) {
                    $this->session->set_flashdata('message', 'Information Updated Successfully.');
                } else {
                    $data ['error'] = 'Something wrong Happen with database;';
                }
            }
        }
        $data ['list'] = $this->leads->getLeadsList();
        $data ['sales'] = $this->leads->getSalesList();
        $this->setData($data);
    }

    public function listLeads() {
        $name = $this->input->post('name');
        $value = $this->input->post('value');
        $data ['list'] = $this->leads->getLeadsSearchList($name, $value);
        $this->load->view('list_leads', $data);
    }

    public function listSales() {
        $salesName = $this->input->post('name');
        $value = $this->input->post('value');
        if ($salesName == 'industry_id')
            $salesName = 'sector_ranking';
        if ($salesName == 'product_interest')
            $salesName = 'product';
        $data ['sales'] = $this->leads->getSalesSearchList($salesName, $value);
        $this->load->view('list_sales', $data);
    }

    /*
     * Client Profile
     */

    public function profile($clientID = '2') {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js'
        );
        $includeCss = array(
            'assets/css/plugins/iCheck/custom.css'
        );
        $data ['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data ['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data ['meta_title'] = "Bank BRI Client Profile";
        $data ['meta_description'] = "Bank BRI";
        $data ['meta_keyword'] = "Bank BRI";
        $data ['lead'] = $this->leads->getLeadInfo($clientID);
        $data ['clients'] = $this->leads->getLeadList();

        $this->setData($data);
    }

    public function addrefferals() {
        echo 123;
        exit();
    }

}

?>