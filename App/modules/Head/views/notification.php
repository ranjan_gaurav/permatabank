<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>Notifications: Head Office</h2></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12 col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content-new refferalcsshead">

                    <div class="clearfix"></div>
                    <div class="content-scroll">
                        <div class="full-height-scroll">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Mark</th>
                                            <th>Date</th>
                                            <th>Alert Type</th>
                                            <th>Alert  Description</th>
                                            <th>Region</th>
                                            <th>Area</th>
                                            <th>Team</th>
                                            <th>PIC</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="gradeX">
                                            <td><input type="checkbox" class="i-checks"></td>
                                            <td>28/04</td>
                                            <td>Issue Flag</td>
                                            <td><a href="#">NPL above average for 3m</a></td>
                                            <td class="center">R1</td>
                                            <td class="center">S&D-001</td>
                                            <td class="center">Team1</td>
                                            <td class="center">Indra</td>

                                        </tr>
                                        <tr class="gradeC">
                                            <td><input type="checkbox" class="i-checks"></td>
                                            <td>27/04</td>
                                            <td>Congratulate</td>
                                            <td><a href="#">Sales increased by 50% since last month</a></td>
                                            <td class="center">R1</td>
                                            <td class="center">S&D-002</td>
                                            <td class="center">Team2</td>
                                            <td class="center">Amit</td>
                                        </tr>
                                        <tr class="gradeA">
                                            <td><input type="checkbox" class="i-checks"></td>
                                            <td>22/04</td>
                                            <td>Issue Flag</td>
                                            <td><a href="#">TAT(Lead to app) in region 1 is 20 days</a></td>
                                            <td class="center">R3</td>
                                            <td class="center">S&D-003</td>
                                            <td class="center">Team3</td>
                                            <td class="center">Sumit</td>
                                        </tr>
                                      
                                        <tr class="gradeC">
                                            <td><input type="checkbox" class="i-checks"></td>
                                            <td>20/04</td>
                                            <td>Congratulate</td>
                                            <td><a href="#">Sales increased by 1000% since last month</a></td>
                                            <td class="center">R2</td>
                                            <td class="center">S&D-004</td>
                                            <td class="center">Team4</td>
                                            <td class="center">Amit</td>
                                        </tr>
                                        <tr class="gradeC">
                                            <td><input type="checkbox" class="i-checks"></td>
                                            <td>28/04</td>
                                            <td>Issue Flag</td>
                                            <td><a href="#">Hunter attention rate in region 3 is below 20%</a></td>
                                            <td class="center">R5</td>
                                            <td class="center">S&D-004</td>
                                            <td class="center">Team5</td>
                                            <td class="center">Gunaguan</td>
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">&nbsp;</div>
                </div>
            </div>
			                    <div class="lead-btns">
                        <!--<button class="btn btn-w-m btn-primary" type="submit">Filter</button>-->
                        <button class="btn btn-w-m btn-warning" type="button">Remove Selected</button>
                    </div>
        </div>
    </div>
</div>

