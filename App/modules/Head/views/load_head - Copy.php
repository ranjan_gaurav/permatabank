<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>RRO Performance Dashboard</h2></div>
</div>
<?php
$hunterInfo = getUserInfo(@$this->session->userdata('userid'));
?>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row padding-bottom-area">
        <div class="col-lg-12 col-sm-12 col-md-12">
            <div class="ibox-content refaral-e refaral-g">
			<div class="col-lg-6 col-sm-6 col-md-6 padding-l1">
                <div class="form-group refaral-form">
                    <label>Level view:</label>
                    <select name="Company_Name" class="selectpicker">
                        <option value="1">National</option>
                        <option value="2">Regional</option>
                        <option value="3">Area</option>
                    </select>
                </div>
            </div>
			<div class="col-lg-6 col-sm-6 col-md-6 padding-r1">
				<div class="form-group refaral-form">
                    <label>Unit / Staff:</label>
                    <select name="Company_Name" class="selectpicker">
                        <option value="1">Team-01</option>
                        <option value="2">Team-02</option>
                        <option value="3">Team-03</option>
                        <option value="4">Team-04</option>
                        <option value="5">Team-05</option>
						<option value="5">Team-06</option>
                    </select>
                </div>
				</div>
				<div class="hr-line-dashed1"></div>
				<div class="col-lg-6 col-sm-6 col-md-6 padding-l1">	
                <div class="form-group refaral-form">
                    <label>PIC</label>
                    <small>Gabelas</small>
                </div>
				</div>
				<div class="col-lg-6 col-sm-6 col-md-6 padding-r1">
                <div class="form-group refaral-form">
                    <label>Region</label>
                    <small>R1</small>
                </div>
                </div>
				<div class="hr-line-dashed1"></div>
				<div class="col-lg-6 col-sm-6 col-md-6 padding-l1">
				<div class="form-group refaral-form">
                    <label>Area</label>
                    <small>Area A</small>
                </div>
				</div>
                <div class="clearfix">&nbsp;</div>
            </div>
        </div>
        
    </div>
                            <!--<div class="col-lg-12 col-md-12 col-sm-12 only-m padding-zy">
                                <div class="information-section">
                                    <div class="heading-l">Last Month Performance </div>
                                    <div class="information-section-area">
                                        <div class="information-section-content">Incentive Score</div>
                                        <div class="information-section-content1">30</div>
                                    </div>
                                    <div class="information-section-area">
                                        <div class="information-section-content">Ranking (Percentile)</div>
                                        <div class="information-section-content1">15%</div>
                                    </div>

                                    <div class="information-section-area">
                                        <div class="information-section-content">Bonus Earned (*)(IDR '000)</div>
                                        <div class="information-section-content1">1,000</div>
                                    </div>
                                </div>

                            </div>-->
                            <?php
                            $array = array();
                            foreach ($result as $arrdata) :
                                array_push($array, $arrdata->ntb);
                            endforeach;
                            ?>
                            <script>
                                var financialntb = [];
                                var ntbwithloadbundle = [];
                                var ntbwithloan = [];
                                var ntbwithoutloan = [];
                                var successfullrefferals = [];
                                var months = [];
<?php foreach ($result as $data) : ?>
                                    financialntb.push(['<?php echo $data->ntb ?>']);
                                    ntbwithloadbundle.push(['<?php echo $data->ntb_with_bundle ?>']);
                                    ntbwithloan.push(['<?php echo $data->ntb_with_loan_only ?>']);
                                    ntbwithoutloan.push(['<?php echo $data->ntb_without_loan ?>']);
                                    successfullrefferals.push(['<?php echo $data->successful_refererrals ?>']);
                                    months.push(['<?php echo date('F', strtotime($data->creation_date)) ?>']);
<?php endforeach; ?>
                            </script>
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-md-12 chart-padding-middle">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Financial and Risk Management KPIs</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="col-lg-4 col-sm-4 chart-padding-right">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Loan Balance Growth</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="loanbunddlechart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Loan_Balance_Growth.svg" class="img-t" alt="financial_kpi" height="100">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-left">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Term Loan Re-Instatement</h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="ntbwithLoanChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Term_Loan_Re-instatement.svg" class="img-t" alt="financial_kpi" height="100">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-left">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>CASA Balance Growth </h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="ntbwithoutLoanChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/CASA_Balance_Growth.svg" class="img-t" alt="financial_kpi" height="100">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-right">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Average CASA Transactions/Customer</h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="refferalChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/head/Average_CASA_Transactions-customer.svg" class="img-t" alt="financial_kpi" height="100">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-left">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Portfolio NPL</h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="ntbwithoutLoanChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/svg-graph/Portfolio_NPL.svg" class="img-t" alt="financial_kpi" height="100">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-sm-4 chart-padding-left">
                                                <div class="grap-content">
                                                    <div class="ibox-title">
                                                        <h5>Cross selling points</h5>

                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="refferalChart">
                                                            <img src="<?php echo base_url(); ?>/assets/img/graph/head/rro_leader.svg" class="img-t" alt="financial_kpi" height="100">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>

                                <!--<div class="col-lg-6 col-sm-6 col-md-6">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Non-Financial KPIs</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="financial_kpi">
                                                                <h5>Risk Management</h5>
                                                                <div class="risk-management">
                                                                <img src="<?php echo base_url(); ?>/assets/img/graph/rrm/Riskmanagement.png" class="img-t" alt="Risk Management">
                                                                </div>
                                                                <div class="line"></div>
                                                 <div id="chartContainer"></div>
                                 <canvas id="ntbChart" height="160"></canvas> 
                                <div class="legends">
                                    <div class="target"><i class="fa fa-square"></i> Target</div>
                                    <div class="achievement"><i class="fa fa-square"></i> Achievement</div>
                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->

                            </div>
                            </div>

