<?php

/*
 *
 */

class Head extends Head_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library("session");
        $this->load->model('head_model', 'head');
        $this->load->helper('url');
    }

    public function index() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/flot/jquery.flot.js',
            'assets/js/plugins/flot/jquery.flot.tooltip.min.js',
            'assets/js/plugins/flot/jquery.flot.spline.js',
            'assets/js/plugins/flot/jquery.flot.resize.js',
            'assets/js/plugins/flot/jquery.flot.pie.js',
            'assets/js/plugins/peity/jquery.peity.min.js',
            'assets/js/demo/peity-demo.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/plugins/jquery-ui/jquery-ui.min.js',
            'assets/js/plugins/gritter/jquery.gritter.min.js',
            'assets/js/plugins/sparkline/jquery.sparkline.min.js',
            'assets/js/demo/sparkline-demo.js',
            'assets/js/plugins/chartJs/Chart.min.js',
            'assets/js/plugins/toastr/toastr.min.js',
            'assets/js/changeTable.js',
            'assets/js/plugins/datapicker/bootstrap-datepicker.js'
                )
        ;
        $includeCss = array(
            'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990',
            'assets/css/plugins/datapicker/datepicker3.css'
                )
        ;
        $data ['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data ['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data ['meta_title'] = "Bank BRI";
        $data ['meta_description'] = "Bank BRI";
        $data ['meta_keyword'] = "Bank BRI";
        $data ['main_content'] = 'index';
        $data ['info'] = $this->head->getheadInfo();
        $data ['result'] = $this->head->getheadData();
        $this->setData($data);
    }

    public function performance() {


        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/flot/jquery.flot.js',
            'assets/js/plugins/flot/jquery.flot.tooltip.min.js',
            'assets/js/plugins/flot/jquery.flot.spline.js',
            'assets/js/plugins/flot/jquery.flot.resize.js',
            'assets/js/plugins/flot/jquery.flot.pie.js',
            'assets/js/plugins/peity/jquery.peity.min.js',
            'assets/js/demo/peity-demo.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/plugins/jquery-ui/jquery-ui.min.js',
            'assets/js/plugins/gritter/jquery.gritter.min.js',
            'assets/js/plugins/sparkline/jquery.sparkline.min.js',
            'assets/js/demo/sparkline-demo.js',
            'assets/js/plugins/chartJs/Chart.min.js',
            'assets/js/plugins/toastr/toastr.min.js',
            // 'assets/js/custom.js',
            'assets/js/load-table.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js'
                )
        // 'assets/js/plugins/dataTables/dataTables.bootstrap.js',
        // 'assets/js/plugins/dataTables/dataTables.responsive.js',
        // 'assets/js/plugins/dataTables/dataTables.tableTools.min.js',

        ;
        $includeCss = array(
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/dataTables/dataTables.responsive.css',
            'assets/css/plugins/dataTables/dataTables.tableTools.min.css'
                )
        ;
        $data ['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data ['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data ['meta_title'] = "Bank BRI";
        $data ['meta_description'] = "Bank BRI";
        $data ['meta_keyword'] = "Bank BRI";
        $data ['main_content'] = 'index';
        $data ['info'] = $this->head->getheadInfo();
        $data ['result'] = $this->head->getheadData();



        $this->setData($data);
    }

    public function rro_performance() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/flot/jquery.flot.js',
            'assets/js/plugins/flot/jquery.flot.tooltip.min.js',
            'assets/js/plugins/flot/jquery.flot.spline.js',
            'assets/js/plugins/flot/jquery.flot.resize.js',
            'assets/js/plugins/flot/jquery.flot.pie.js',
            'assets/js/plugins/peity/jquery.peity.min.js',
            'assets/js/demo/peity-demo.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/plugins/jquery-ui/jquery-ui.min.js',
            'assets/js/plugins/gritter/jquery.gritter.min.js',
            'assets/js/plugins/sparkline/jquery.sparkline.min.js',
            'assets/js/demo/sparkline-demo.js',
            'assets/js/plugins/chartJs/Chart.min.js',
            'assets/js/demo/hunterCharts.js',
            'assets/js/plugins/toastr/toastr.min.js',
            'assets/js/custom.js',
            'assets/js/hunterCharts.js'
        );
        $includeCss = array(
            'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990'
        );
        $data ['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data ['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data ['meta_title'] = "Bank BRI";
        $data ['meta_description'] = "Bank BRI";
        $data ['meta_keyword'] = "Bank BRI";
        $data ['main_content'] = 'index';
        $data ['info'] = $this->head->getheadInfo();
        $data ['result'] = $this->head->getheadData();
        $this->setData($data);
    }

    public function segment() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/peity/jquery.peity.min.js',
            'assets/js/demo/peity-demo.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/datapicker/bootstrap-datepicker.js',
            'assets/js/barChart.jquery.js'
                )
        ;
        $includeCss = array(
            'assets/css/barchart.css',
            'assets/css/plugins/datapicker/datepicker3.css'
        );
        $data ['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data ['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data ['meta_title'] = "Bank BRI Head SME Sales Toolkit";
        $data ['meta_description'] = "Bank BRI Head";
        $data ['meta_keyword'] = "Bank BRI Head";
        $data ['main_content'] = 'index';
        $this->setData($data);
    }

    public function notification() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/bootbox.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/lead_directory.js'
        );
        $includeCss = array(
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/dataTables/dataTables.responsive.css',
            'assets/css/plugins/dataTables/dataTables.tableTools.min.css',
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
            'assets/css/plugins/codemirror/codemirror.css',
            'assets/css/plugins/codemirror/ambiance.css'
        );
        $data ['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data ['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data ['meta_title'] = "Bank BRI Head SME Sales Toolkit";
        $data ['meta_description'] = "Bank BRI Head";
        $data ['meta_keyword'] = "Bank BRI Head";
        $data ['main_content'] = 'index';
        $this->setData($data);
    }

    public function load() {
        error_reporting(0);

        $q ['val'] = intval($_GET ['q']);

        $this->load->view('data-table', $q);
    }

    public function change_table() {
        //error_reporting ( 0 );

        $data['val'] = $this->input->post('id');

        //print_r($data['val']);
        //exit;

        $data['js_to_load'] = "load-table.js";
        $this->load->view('load-table', $data);
        //$this->setData ( $data );
    }

    public function process() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/flot/jquery.flot.js',
            'assets/js/plugins/flot/jquery.flot.tooltip.min.js',
            'assets/js/plugins/flot/jquery.flot.spline.js',
            'assets/js/plugins/flot/jquery.flot.resize.js',
            'assets/js/plugins/flot/jquery.flot.pie.js',
            'assets/js/plugins/peity/jquery.peity.min.js',
            'assets/js/demo/peity-demo.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/plugins/jquery-ui/jquery-ui.min.js',
            'assets/js/plugins/gritter/jquery.gritter.min.js',
            'assets/js/plugins/sparkline/jquery.sparkline.min.js',
            'assets/js/demo/sparkline-demo.js',
            'assets/js/plugins/chartJs/Chart.min.js',
            'assets/js/demo/hunterCharts.js',
            'assets/js/plugins/toastr/toastr.min.js',
            'assets/js/custom.js',
            'assets/js/hunterCharts.js'
        );
        $includeCss = array(
            'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990'
        );
        $data ['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data ['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data ['meta_title'] = "Bank BRI";
        $data ['meta_description'] = "Bank BRI";
        $data ['meta_keyword'] = "Bank BRI";
        $data ['main_content'] = 'index';
        $data ['info'] = $this->head->getHunterInfo();
        $data ['result'] = $this->head->getHunterData();
        $this->setData($data);
    }

    public function load_head() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/flot/jquery.flot.js',
            'assets/js/plugins/flot/jquery.flot.tooltip.min.js',
            'assets/js/plugins/flot/jquery.flot.spline.js',
            'assets/js/plugins/flot/jquery.flot.resize.js',
            'assets/js/plugins/flot/jquery.flot.pie.js',
            'assets/js/plugins/peity/jquery.peity.min.js',
            'assets/js/demo/peity-demo.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/plugins/jquery-ui/jquery-ui.min.js',
            'assets/js/plugins/gritter/jquery.gritter.min.js',
            'assets/js/plugins/sparkline/jquery.sparkline.min.js',
            'assets/js/demo/sparkline-demo.js',
            'assets/js/plugins/chartJs/Chart.min.js',
            'assets/js/plugins/toastr/toastr.min.js',
            //	'assets/js/custom.js',
            'assets/js/load-table.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js'
        );
        $includeCss = array(
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/dataTables/dataTables.responsive.css',
            'assets/css/plugins/dataTables/dataTables.tableTools.min.css'
        );
        $data ['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data ['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data ['meta_title'] = "Bank BRI";
        $data ['meta_description'] = "Bank BRI";
        $data ['meta_keyword'] = "Bank BRI";
        $data ['main_content'] = 'index';
        $data ['info'] = $this->head->getHunterInfo();
        $data ['result'] = $this->head->getHunterData();

        $this->setData($data);
    }

}

?>