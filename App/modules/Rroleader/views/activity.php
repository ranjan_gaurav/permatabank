<script>
    var activities = [];
<?php foreach ($result as $data) : ?>
        var data = {
            id: <?php echo $data->activity_id ?>,
            title: "<?php echo $data->name ?>",
            start: new Date("<?php echo date("F d, Y H:i:s", strtotime($data->start_date)) ?>"),
            end: new Date("<?php echo date("F d, Y H:i:s", strtotime($data->end_date)) ?>"),
            allDay: false
        };
        activities.push(data);
        var base_url = "<?php echo base_url(); ?>";
<?php endforeach; ?>
</script>
<?php if ($this->session->flashdata('message')) { ?>
    <div class="panel panel-success" id="flash-message">
        <div class="panel-heading">
            Success Panel
        </div>
        <div class="panel-body">
            <p><?php echo $this->session->flashdata('message'); ?></p>
        </div>
    </div>
<?php } ?>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Activity Planner</h5>
                </div>
                <div class="ibox-content">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="calenderActivity" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm1">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Add Events</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" id="calenderForm">

                    <div class="col-sm-12 col-md-12">

                        <div class="form-group lead-forms2">
                            <label>Company Name:</label>
                            <select name="company" class="form-control" required>
                                <option value="">Select Company Name</option>
                                <option value="Aluminium Light Metal Indonesia">Aluminium Light Metal Indonesia</option>
                                <option value="Jindal Stainless Steel">Jindal Stainless Steel</option>
                                <option value="Steel Pipe Industry Indonesia">Steel Pipe Industry Indonesia</option>
                                <option value="Ispat Indo">Ispat Indo</option>
                                <option value="Papajaya Agung">Papajaya Agung</option>
                                <option value="Total Bangun Persada">Total Bangun Persada</option>
                                <option value="Hutama Karya">Hutama Karya</option>
                                <option value="Wijaya Karya ">Wijaya Karya </option>
                                <option value="Adhi Karya">Adhi Karya</option>
                                <option value="Pembangunan Perumahan">Pembangunan Perumahan</option>
                            </select>
                        </div>
                        <div class="form-group lead-forms2">
                            <label class="">Activity:</label>
                            <select name="activity" class="form-control">
                                <option value="">Select Activity</option>
                                <option value="Visit">Visit</option>
                                <option value="Call">Call</option>
                                <option value="Mail">Mail</option>
                            </select>
                        </div>
                        <div class="form-group lead-forms2">
                            <label>Purpose</label>
                            <select name="purpose" class="form-control">
                                <option value="Initial contact">Initial Contact</option>
                                <option value="Follow-up">Follow-Up</option>
                                <option value="Application Preparation">Application Preparation</option>
                                <option value="Application re-work">Application Re-work</option>
                                <option value="Onboarding">Onboarding</option>
                            </select>
                        </div>
                        <div class="form-group lead-forms2">
                            <label>Start Time :</label>
                            <input type="text" name="start_dates" class="form-control" placeholder="Start" aria-required="true" id="starts"> 
                        </div>
                        <div class="form-group lead-forms2">
                            <label>End Time :</label>
                            <input type="text" name="end_dates" class="form-control" placeholder="Ends" aria-required="true" id="ends">

                        </div>

                        <div class="input-group space-top col-sm-12">
                            <label class="control-label">Note </label>
                            <textarea rows="4" cols="50" class="form-control" width="100%" name="description">

                            </textarea>
                        </div>
                        <div class="clearfix">&nbsp;</div>

                    </div>
                    <?php /*   <div class="form-group popup-form"><label class="col-sm-2 control-label">Name</label>
                      <div class="col-sm-10"><input class="form-control required" type="text" name="name"></div>
                      </div>
                      <div class="form-group popup-form"><label class="col-sm-2 control-label">URL</label>
                      <div class="col-sm-10"><input class="form-control required" type="text" name="url"></div>
                      </div>
                      <div class="form-group popup-form"><label class="col-sm-2 control-label">Notes</label>
                      <div class="col-sm-10"><textarea class="form-control required" type="text" name="description"></textarea></div>
                      </div>
                      <div class="form-group popup-form">
                      <div class="date-picer">
                      <label class="col-sm-4 control-label">Starts</label>
                      <div class="col-sm-8">
                      <div class="input-group clockpicker" data-autoclose="true">
                      <input type="text" class="form-control" value="" id="starts" name="start_date">
                      =======
                      <div class="col-sm-12 col-md-12">

                      <div class="form-group lead-forms2">
                      <label>Company Name :</label>
                      <select name="company" class="form-control" required>
                      <option value="">Select Company Name</option>
                      <option value="Aluminium Light Metal Indonesia">Aluminium Light Metal Indonesia</option>
                      <option value="Jindal Stainless Steel">Jindal Stainless Steel</option>
                      <option value="Steel Pipe Industry Indonesia">Steel Pipe Industry Indonesia</option>
                      <option value="Ispat Indo">Ispat Indo</option>
                      <option value="Papajaya Agung">Papajaya Agung</option>
                      <option value="Total Bangun Persada">Total Bangun Persada</option>
                      <option value="Hutama Karya">Hutama Karya</option>
                      <option value="Wijaya Karya ">Wijaya Karya </option>
                      <option value="Adhi Karya">Adhi Karya</option>
                      <option value="Pembangunan Perumahan">Pembangunan Perumahan</option>
                      </select>
                      </div>
                      <div class="form-group lead-forms2">
                      <label class="">Activity :</label>
                      <select name="activity" class="form-control">
                      <option value="">Select Activity</option>
                      <option value="Visit">Visit</option>
                      <option value="Call">Call</option>
                      <option value="Mail">Mail</option>
                      </select>
                      </div>
                      <div class="form-group lead-forms2">
                      <label>Purpose :</label>
                      <select name="purpose" class="form-control">
                      <option value="Initial contact">Initial Contact</option>
                      <option value="Follow-up">Follow-Up</option>
                      <option value="Application Preparation">Application Preparation</option>
                      <option value="Application re-work">Application Re-work</option>
                      <option value="Onboarding">Onboarding</option>
                      </select>
                      </div>
                      <div class="form-group lead-forms2">
                      <label>Start Time :</label>
                      <input type="text" name="start_dates" class="form-control" placeholder="Start" aria-required="true" id="starts">
                      </div>
                      <div class="form-group lead-forms2">
                      <label>End Time :</label>
                      <input type="text" name="end_dates" class="form-control" placeholder="Ends" aria-required="true" id="ends">
                      >>>>>>> 317706b83d3bf20679c2474c85b8a6479c467312
                      </div>

                      <div class="input-group space-top col-sm-12">
                      <label class="control-label">Note :</label>
                      <textarea rows="4" cols="50" class="form-control" width="100%" name="description">

                      </textarea>
                      </div>
                      <<<<<<< HEAD
                      </div>
                      </div>
                      </div>
                      <div class="form-group">
                      =======


                      </div>
                      <?php /*   <div class="form-group popup-form"><label class="col-sm-2 control-label">Name</label>
                      <div class="col-sm-10"><input class="form-control required" type="text" name="name"></div>
                      </div>
                      <div class="form-group popup-form"><label class="col-sm-2 control-label">URL</label>
                      <div class="col-sm-10"><input class="form-control required" type="text" name="url"></div>
                      </div>
                      <div class="form-group popup-form"><label class="col-sm-2 control-label">Notes</label>
                      <div class="col-sm-10"><textarea class="form-control required" type="text" name="description"></textarea></div>
                      </div>
                      <div class="form-group popup-form">
                      <div class="date-picer">
                      <label class="col-sm-4 control-label">Starts</label>
                      <div class="col-sm-8">
                      <div class="input-group clockpicker" data-autoclose="true">
                      <input type="text" class="form-control" value="" id="starts" name="start_date">
                      </div>
                      </div>
                      </div>
                      <div class="date-picer">
                      <label class="col-sm-4 control-label">Ends</label>
                      <div class="col-sm-8">
                      <div class="input-group clockpicker" data-autoclose="true">
                      <input type="text" class="form-control" value="" id="ends" name="end_date">
                      </div>
                      </div>
                      </div>
                      </div>
                      <div class="form-group">
                      >>>>>>> 317706b83d3bf20679c2474c85b8a6479c467312
                      <div class='input-group date' id='datetimepicker1'>
                      <input type='text' class="form-control" id='datetimepicker4' />
                      </div>
                      </div> */ ?>

                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-w-m btn-primary pull-right btn-m-top">Create</button>
                    </div>
                    <div class="clearfix">&nbsp;</div>
                </form>
            </div>
        </div>

    </div>
</div>

<div class="modal inmodal" id="calenderSingleActivity" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title cal_activity_title">Activity Title</h4>
            </div>
            <div class="modal-body">
                <div class="form-group popup-form1">
                    <label class="col-sm-4 control-label">Company :</label>
                    <div class="col-sm-8">
                        <div id="cal_activity_company"> </div>
                    </div>
                </div>
                <div class="form-group popup-form1">
                    <label class="col-sm-4 control-label">Purpose :</label>
                    <div class="col-sm-8">
                        <div id="cal_activity_purpose"> </div>
                    </div>
                </div>
                <div class="form-group popup-form1">
                    <label class="col-sm-4 control-label">Note</label>
                    <div class="col-sm-8">
                        <div id="cal_activity_notes"></div>
                    </div>
                </div>
                <div class="form-group popup-form1">
                    <label class="col-sm-4 control-label">Start Time</label>
                    <div class="col-sm-8">
                        <div id="cal_activity_start_time"></div>
                    </div>
                </div>
                <div class="form-group popup-form1">
                    <label class="col-sm-4 control-label">End Time</label>
                    <div class="col-sm-8">
                        <div id="cal_activity_end_time"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
</div>