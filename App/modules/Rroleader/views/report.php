<?php
/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 5/4/2016
 * Time: 6:56 PM
 */
?>

<?php //print_r($this->input->post());      ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>Activity Report</h2></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins activity-report-label">
                <?php /* if (@$error): ?>
                  <div class="panel panel-warning">
                  <div class="panel-heading">
                  <i class="fa fa-warning"></i> Warning Panel
                  </div>
                  <div class="panel-body">
                  <p><?php //echo $error;    ?>Please fill all mandatory fields.</p>
                  </div>
                  </div>
                  <?php endif; */ ?>
                <?php if ($this->session->flashdata('message')) { ?>
                    <div class="panel panel-success " id="flash-message">
                        <div class="panel-heading">
                            Success Panel
                        </div>
                        <div class="panel-body">
                            <p><?php echo $this->session->flashdata('message'); ?></p>
                        </div>
                    </div>
                <?php } ?>
                <form method="post" class="form-horizontal" id="calenderForm">
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <div class="ibox-content">
                                <div class="height-min">
                                    <div class="form-group lead-forms2 follow-up1">
                                        <div class="add-st">
                                            <label>Status</label>
                                            <select name="status" class="selectpicker">
                                                <option value="">Status</option>
                                                <option value="Unallocated">Unallocated</option>
                                                <option value="Allocated">Allocated</option>
                                                <option value="Engaged">Engaged</option>
                                                <option value="Application Ongoing">Application Ongoing</option>
                                                <option value="Pending Approval">Pending Approval</option>
                                                <option value="Onboarding">Onboarding</option>
                                                <option value="Withdrawn">Withdrawn</option>
                                                <option value="Rejected">Rejected</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="follow-up">Meeting Schedule</div>
                                    <div class="form-group lead-forms2">
                                        <label>Company Name</label>
                                        <select name="company" class="selectpicker">
                                            <option value="">Select Company Name</option>
                                            <option value="Company A" <?php
                                            if ($this->input->post('lead') == '4') {
                                                echo "selected";
                                            }
                                            ?>>Company A</option>
                                            <option value="Company B" <?php
                                            if ($this->input->post('lead') == '5') {
                                                echo "selected";
                                            }
                                            ?>>Company B</option>
                                            
                                            <option value="Company C" <?php
                                            if ($this->input->post('lead') == '7') {
                                                echo "selected";
                                            }
                                            ?>>Company C</option>
                                            
                                            <option value="Company D" <?php
                                            if ($this->input->post('lead') == '1') {
                                                echo "selected";
                                            }
                                            ?>>Company D</option>
                                            <option value="Company E" <?php
                                            if ($this->input->post('lead') == '2') {
                                                echo "selected";
                                            }
                                            ?>>Company E</option>
                                        </select>
                                    </div>
                                    <div class="form-group lead-forms2">
                                        <label class="red-high">Activity*</label>
                                        <select name="name" class="selectpicker">
                                            <option value="">Select Activity</option>
                                            <option value="Visit">Visit</option>
                                            <option value="Call">Call</option>
                                        </select>
                                    </div>
                                    <div class="form-group lead-forms2">
                                        <label>Purpose</label>
                                        <select name="purpose" class="selectpicker">
                                            <option value="Initial contact">Initial Contact</option>
                                            <option value="Follow-up">Follow-Up</option>
                                            <option value="Application Preparation">Application Preparation</option>
                                            <option value="Application re-work">Application Re-work</option>
                                            <option value="Onboarding">Onboarding</option>
                                        </select>
                                    </div>
                                    <div class="form-group lead-forms2">
                                        <label class="red-high">Start*</label>
                                        <div id="datetimepicker1" class="input-append date">
                                            <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class="form-control required" name="start_date" placeholder="Starts"></input>
                                            <span class="add-on">
                                                <i class="icon-calendar" data-date-icon="icon-calendar" data-time-icon="icon-time">
                                                </i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group lead-forms2">
                                        <label class="red-high">End*</label>
                                        <div id="datetimepicker2" class="input-append date2">
                                            <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class="form-control required" name="end_date" placeholder="Ends"></input>
                                            <span class="add-on">
                                                <i class="icon-calendar" data-date-icon="icon-calendar" data-time-icon="icon-time">
                                                </i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="ibox-content">
                                <div class="height-min">
                                    <div class="form-group lead-forms2 follow-up1">
                                        <div class="add-st">
                                            <label>Priority</label>
                                            <select name="priority" class="selectpicker">
                                                <option value="">Select Priority</option>
                                                <option value="1">High</option>
                                                <option value="2">Medium</option>
                                                <option value="3">Low</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="follow-up">Follow-up Meeting Schedule</div>
                                    <div class="form-group lead-forms2">
                                        <label>Activity</label>
                                        <select class="selectpicker">
                                            <option value="">Select Activity</option>
                                            <option value="Visit">Visit</option>
                                            <option value="Call">Call</option>
                                        </select>
                                    </div>
                                    <div class="form-group lead-forms2">
                                        <label>Purpose</label>
                                        <select class="selectpicker">
                                            <option value="Initial contact">Initial Contact</option>
                                            <option value="Follow-up">Follow-Up</option>
                                            <option value="Application Preparation">Application Preparation</option>
                                            <option value="Application re-work">Application Re-work</option>
                                            <option value="Onboarding">Onboarding</option>
                                        </select>
                                    </div>
                                    <div class="form-group lead-forms2">
                                        <label>Start</label>
                                        <div id="datetimepicker3" class="input-append date">
                                            <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class="form-control" placeholder="Starts"></input>
                                            <span class="add-on">
                                                <i class="icon-calendar" data-date-icon="icon-calendar" data-time-icon="icon-time">
                                                </i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group lead-forms2">
                                        <label>End</label>
                                        <div id="datetimepicker4" class="input-append date">
                                            <input data-format="yyyy-MM-dd hh:mm:ss" type="text" class="form-control" placeholder="Ends"></input>
                                            <span class="add-on">
                                                <i class="icon-calendar" data-date-icon="icon-calendar" data-time-icon="icon-time">
                                                </i>
                                            </span>
                                        </div> 
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12">

                            <div class="input-group space-top col-sm-12">
                                <label class="control-label">Report</label>
                                <textarea rows="4" cols="50" class="form-control" width="100%" name="description"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="lead-btns">
                                <button type="submit" class="btn btn-primary btn-w-m">Save</button>
                                <?php if ($this->input->post('back_url')) { ?>
                                    <a href="<?php echo $this->input->post('back_url'); ?>" class="btn btn-w-m btn-info">Cancel</a>
                                <?php } ?>
                            </div>

                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>


            </div>
        </div>
    </div>
</div>