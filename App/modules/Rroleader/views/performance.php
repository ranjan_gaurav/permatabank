<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>RRO Leader Performance Dashboard</h2></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12 col-sm-12 p-d-b">
            <div class="col-lg-6 col-sm-12 col-md-8 padding-l">
                <!--- date picker--->
                <div class="col-lg-6 col-sm-6 col-md-6 padding-l">
                    <div class="form-group" id="data_3">
                        <label class="font-noraml">Start Date</label>
                        <div class="form-group" id="performance_decade_view1">
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input class="form-control" value="06/06/2016" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6 padding-l">
                    <div class="form-group" id="data_3">
                        <label class="font-noraml">End Date</label>
                        <div class="form-group" id="performance_decade_view2">
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input class="form-control" value="06/06/2016" type="text">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--             <div class="col-lg-6 col-sm-3 col-md-4 padding-r"> -->
                  <!--            <div class="top-btn-sec"><a href="<?php //echo base_url('rroleader');  ?>" class="btn btn-w-m btn-info">Summary</a></div> -->
            <!--             </div> -->
        </div>
        <div class="col-lg-8 col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content hun-performance">
                    <div class="content demo-yx">
                        <div class="table-responsive24">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th colspan="6" class="text-cen">Financial and Risk Management KPIs</th>
                                        <th colspan="3" class="text-cen">Process Indicator</th>
                                    </tr>
                                    <tr>
                                        <th rowspan="" valign="middle">RRO Name</th>
                                        <th>Loan Balance Growth</th>
                                        <th>Term Loan Re-Installment</th>
                                        <th>CASA Balance Growth</th>
                                        <th>Average CASA Transaction/Customer</th>
                                        <th>Portfolio NPL</th>
                                        <th>Cross Selling Point</th>
                                        <th>Task Succeeded</th>
                                        <th>Task Failed</th>
                                        <th>Task Pending</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($result as $list) : ?>
                                        <tr class="gradeX">
                                            <td><a id="<?php echo $list->userid; ?>" onclick="changeProfileInformation(this.id);"><?php echo $list->rro_name; ?></a></td>
                                            <td><?php echo $list->loan_balance; ?></td>
                                            <td><?php echo $list->term_loan; ?></td>
                                            <td><?php echo $list->casa_balance; ?></td>
                                            <td><?php echo $list->avg_casa; ?></td>
                                            <td><?php echo $list->portfolio; ?></td>
                                            <td><?php echo $list->cross_selling; ?></td>
                                            <td><?php echo $list->task; ?></td>
                                            <td><?php echo $list->task_failed; ?></td>
                                            <td><?php echo $list->task_pending; ?></td>
                                        </tr>
                                    <?php endforeach; ?>

                                    <tr class="gradeX">
                                        <td>Team (MTD)</td>
                                        <td>2.5%</td>
                                        <td>13.0%</td>
                                        <td>4.8%</td>
                                        <td>4.625</td>
                                        <td>3.3%</td>
                                        <td>22</td>
                                        <td>55.0%</td>
                                        <td>27.5%</td>
                                        <td>17.5%</td>
                                    </tr>

                                </tbody>



                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content alerts">
                    <div class="content demo-yx">
                        <div class="table-responsive25">
                            <table class="table table-striped table-bordered table-hover dataTables">
                                <thead>
                                    <tr>
                                        <th class="text-cen" colspan="4">Alerts</th>

                                    </tr>
                                    <tr>
                                        <th>Type</th>
                                        <th>Description</th>
                                        <th>Escalated to</th>
                                        <th>Delete</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="gradeX">
                                        <td>KPI</td>
                                        <td>3mth RRM Fail to Meet KPIs</td>
                                        <td>Area manager</td>
                                        <td class="center">
                                            <a name="5" class="withdraw" href="javascript:void(0);">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </td>

                                    </tr>
                                    <tr class="gradeC">
                                        <td>Loan Maintenance</td>
                                        <td>Failure to Renew Loan / Overdraft</td>
                                        <td>RRM Leader</td>
                                        <td class="center">
                                            <a name="5" class="withdraw" href="javascript:void(0);">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </td>

                                    </tr>
                                    <tr class="gradeA">
                                        <td>Activity</td>
                                        <td>Fail to Engage Lead by First 2wks</td>
                                        <td>RRM Leader</td>
                                        <td class="center">
                                            <a name="5" class="withdraw" href="javascript:void(0);">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </td>

                                    </tr>
                                    <tr class="gradeA">
                                        <td>Loan Maintenance</td>
                                        <td>Missed Loan Payment</td>
                                        <td>Area manager</td>
                                        <td class="center">
                                            <a name="5" class="withdraw" href="javascript:void(0);">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </td>

                                    </tr>
                                    <tr class="gradeA">
                                        <td>KPI</td>
                                        <td>3mth RRM Fail to Meet KPIs</td>
                                        <td>Area manager</td>
                                        <td class="center">
                                            <a name="5" class="withdraw" href="javascript:void(0);">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </td>

                                    </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<div id="myModal" class="modal">
    <div id="modal-content" class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
                    aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">
            <input type="hidden" id="P_reqesturl"
                   value="<?php echo base_url('rroleader/user_info'); ?>" /> 
                   <?php
                   $this->load->view('modalperformance');
                   ?>
        </div>
    </div>
</div>
