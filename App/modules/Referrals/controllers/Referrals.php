<?php

/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 5/4/2016
 * Time: 6:56 PM
 */
class Referrals extends Front_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library("session");
        $this->load->model('referrals_model', 'referrals');
    }

    public function index() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/plugins/validate/jquery.validate.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/bootbox.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/referrals.js',
            'assets/js/refferalsdelete.js'
        );
        $includeCss = array(
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/steps/jquery.steps.css'
        );

        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('lead_name', 'Name', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data ['error'] = validation_errors();
        } else {
            $insert=$this->referrals->add_reffrals();
            if($insert){
                redirect(base_url('referrals/tracking'));
            }
        }
        $data['meta_title'] = "Bank BRI Refferals";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $this->setData($data);
    }

    public function tracking() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assetsjs/plugins/iCheck/icheck.min.js',
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/bootbox.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/tracking.js',
        );
        $includeCss = array(
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/dataTables/dataTables.responsive.css',
            'assets/css/plugins/dataTables/dataTables.tableTools.min.css',
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
            'assets/css/plugins/codemirror/codemirror.css',
            'assets/css/plugins/codemirror/ambiance.css',
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data['meta_title'] = "Danamon Referrals Tracking Page";
        $data['meta_description'] = "Danamon";
        $data['meta_keyword'] = "Danamon";
        $data ['result'] = $this->referrals->getReffralsList();
        $data ['count'] = $this->referrals->count();

        $this->setData($data);
    }

    public function deleteReffrals() {
        $id = $this->input->post('reffer_id');
        $this->referrals->deletereff($id);
        $data ['status'] = true;
        $data ['message'] = "Withdrawn Successfully";
        echo json_encode($data);
    }

}

?>