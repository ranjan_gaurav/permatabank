<?php

/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 5/4/2016
 * Time: 6:56 PM
 */
class Referrals_model extends CI_Model {

    var $table = "reffers";
    var $company_table = "company";

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    public function getReffralsList() {
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function deletereff($id) {
        $this->db->where('reffer_id', $id);
        $this->db->delete('reffers');
    }

    public function count() {
        $count = $this->db->count_all_results('reffers');
        return $count;
    }

    public function add_reffrals() {
        $leadId=$this->db->select_max('lead_id')->from($this->table)->get()->row()->lead_id;
        $data=array(
            'lead_id'=>$leadId+1,
            'lead_name'=>$this->input->post('lead_name'),
            'address'=>$this->input->post('address'),
            'phone'=>$this->input->post('phone'),
            'email'=>$this->input->post('email'),
            'comment'=>$this->input->post('comment'),
            'status'=>$this->input->post('status'),
            'refer_to'=>$this->input->post('refer_to'),
            'industry'=>$this->input->post('industry'),
            'rm'=>'Ivan Cristanto',
            'branch'=>'Branch 1',
        );
        $this->db->set('reffral_date', 'NOW()', false);
        $query = $this->db->insert($this->table, $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

}

?>