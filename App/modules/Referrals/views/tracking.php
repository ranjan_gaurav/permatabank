<?php if ($this->session->flashdata('message')) { ?>
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
    </div>
    <?php } ?>
    
  
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10"><h2>Referral Tracking</h2></div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12 col-sm-12">
    <div class="pull-right">
		<div class="label-nd">
		<label class="">Potential Reward (MTD)</label>
		<input type="text" placeholder="Enter Name" readonly="readonly" class="form-control required" name="name" value="<?php echo $count ;?>">
	</div>
	</div>
            <div class="ibox float-e-margins">
                <div class="ibox-content-new refferalcssh">

                    <div class="clearfix"></div>
                    <div class="content-scrols">
                        <div class="content demo-yx">
                            <div class="table-responsive28">
                                <table class="table table-striped table-bordered table-hover dataTables-example">
                                    <thead>
                                    
                                        <tr>
                                            <th>No</th>
                                            <th>Lead ID</th>
                                            <th>Lead Name</th>
                                            <th>Referral Date</th>
                                            <th>RM</th>
                                            <th>Branch</th>
                                            <th>Status</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $counter =0;?>
                                     <?php foreach ($result as $list) : ?>
                                        <tr class="gradeU">
                                            <td> <?php echo ++$counter; ?></td>
                                            <td><?php echo $list->lead_id; ?></td>
                                            <td><?php echo $list->lead_name; ?></td>
                                            <td><?php echo $list->reffral_date; ?></td>
                                            <td class="center"><?php echo $list->rm; ?></td>
                                            <td class="center"><?php echo $list->Branch; ?></td>
                                            <td class="center"><?php echo $list->status; ?></td>
                                           
                                            
                                        
                                        </tr>
                                       <?php endforeach; ?>


                                    </tbody>
                                </table>
								<input type="hidden" id="P_withdrawurl" value="<?php echo base_url('referrals/deleteReffrals'); ?>" />
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>