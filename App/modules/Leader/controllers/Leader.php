<?php

/*

 */

class Leader extends Front_Controller {

    function index() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/flot/jquery.flot.js',
            'assets/js/plugins/flot/jquery.flot.tooltip.min.js',
            'assets/js/plugins/flot/jquery.flot.spline.js',
            'assets/js/plugins/flot/jquery.flot.resize.js',
            'assets/js/plugins/flot/jquery.flot.pie.js',
            'assets/js/plugins/peity/jquery.peity.min.js',
            'assets/js/demo/peity-demo.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/plugins/jquery-ui/jquery-ui.min.js',
            'assets/js/plugins/gritter/jquery.gritter.min.js',
            'assets/js/plugins/sparkline/jquery.sparkline.min.js',
            'assets/js/demo/sparkline-demo.js',
            'assets/js/plugins/chartJs/Chart.min.js',
            'assets/js/plugins/toastr/toastr.min.js',
            'assets/js/custom.js'
        );
        $includeCss = array(
            'assets/css/theme-default/libs/DataTables/jquery.dataTables.css?1423553989',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css?1423553990',
            'assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css?1423553990',
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['meta_title'] = "Bank BRI Leader";
        $data['meta_description'] = "Bank BRI Leader";
        $data['meta_keyword'] = "Bank BRI Leader";
        $data['main_content'] = 'index';
        $this->setData($data);
    }

    function performance() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/bootbox.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/lead_directory.js'
        );
        $includeCss = array(
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/dataTables/dataTables.responsive.css',
            'assets/css/plugins/dataTables/dataTables.tableTools.min.css',
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css'
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data['meta_title'] = "Bank BRI Leader Performance";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $this->setData($data);
    }

    function summary() {
        $includeJs = array(
            'assets/js/plugins/metisMenu/jquery.metisMenu.js',
            'assets/js/plugins/slimscroll/jquery.slimscroll.min.js',
            'assets/js/plugins/jeditable/jquery.jeditable.js',
            'assets/js/plugins/dataTables/jquery.dataTables.js',
            'assets/js/plugins/dataTables/dataTables.bootstrap.js',
            'assets/js/plugins/dataTables/dataTables.responsive.js',
            'assets/js/plugins/dataTables/dataTables.tableTools.min.js',
            'assets/js/inspinia.js',
            'assets/js/plugins/pace/pace.min.js',
            'assets/js/bootbox.js',
            'assets/js/plugins/iCheck/icheck.min.js',
            'assets/js/lead_directory.js'
        );
        $includeCss = array(
            'assets/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets/css/plugins/dataTables/dataTables.responsive.css',
            'assets/css/plugins/dataTables/dataTables.tableTools.min.css',
            'assets/css/plugins/iCheck/custom.css',
            'assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css'
        );
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss, 'header');
        $data['meta_title'] = "Bank BRI Leader Summary";
        $data['meta_description'] = "Bank BRI";
        $data['meta_keyword'] = "Bank BRI";
        $this->setData($data);
    }

}

?>