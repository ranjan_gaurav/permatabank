<?php

/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 5/4/2016
 * Time: 6:56 PM
 */
class Sales_model extends CI_Model {

    var $table = "lead";
    var $company_table = "company";
    var $documents_table = "application_documents";
    var $product_table = "products";
    var $sub_product_table = "sub_products";
    var $cat_table = "type_category";

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    public function documentsList() {
        $this->db->distinct();
        $this->db->select("document_name");
        $name = $this->input->post('sub_products1');
        $proIDs=array();
        if ($this->input->post('main_product_ids'))
        {
            $proIDs = explode(',', $this->input->post('main_product_ids'));
            if (!empty($proIDs))
            {
            	$this->db->where_in('product_id', $proIDs);
            }
        }
        
        
       
        $res = $this->db->get($this->documents_table);
      //  echo $this->db->last_query();
        return $res->result();
    }

    public function insertDocumentDetails() {
        $id = $this->input->post('document_id');
        $name = $this->input->post('document_name');
        $this->db->where('document_id', $id);
        $data = array('document_name' => $name);
        $this->db->update($this->documents_table, $data);
    }

    /*public function GetProductsListing() {
        $IDs = $this->input->post('sub_products');
        if (!empty($IDs))
            $this->db->where_in('sub_product_id', $IDs);
        $this->db->from($this->sub_product_table . ' sub');
        $this->db->join($this->product_table . ' pro', 'pro.product_id=sub.main_product_id');
        $this->db->join($this->cat_table . ' cat', 'cat.cat_id=sub.type');
        $this->db->group_by('main_product_id');
        $query = $this->db->get();
        echo $this->db->last_query();
        return $query->result();
    }*/
    
    public function GetProductsListing() {
        //print_r(explode(',',$this->input->post('main_product_ids')));
        $IDs = explode(',',$this->input->post('main_product_ids'));
       // $check = '';
        $name = $this->input->post('sub_products');
     
        $check = implode(',',$name);
        $value = explode(',',$check);
   
        if (!in_array('',$IDs))
        {
        $this->db->where_in('product_id', $IDs);
        $this->db->from($this->product_table . ' pro');
        $query = $this->db->get();
     //   echo $this->db->last_query(); DIE();
        }
        
        else {
        	
        	$this->db->where_in(sub_product_id, $value);
        	$this->db->select('sub_product_name as product_name,sub_product_id as product_id');
        	$this->db->from($this->sub_product_table);
        	$query = $this->db->get();
        	
        	//echo $this->db->last_query(); DIE();
        }
       // echo $this->db->last_query();
        return $query->result();
    }
    
    public function GetAdditionalProductsListing() {
        //print_r(explode(',',$this->input->post('main_product_ids')));
        $IDs = explode(',',$this->input->post('additional_ids'));//$this->input->post('main_product_ids');
        
        $name = $this->input->post('sub_products1');
        $check = implode(',',$name);
        $value = explode(',',$check);
 
        if (!in_array('',$IDs))
        {
        $this->db->where_in('product_id', $IDs);
        $this->db->from($this->product_table . ' pro');
        $query = $this->db->get();
        }
        
        else 
        {
        	
        	$this->db->where_in('sub_product_id', $value);
        	$this->db->select('sub_product_name as product_name,sub_product_id as product_id');
        	$this->db->from($this->sub_product_table);
        	//echo $this->db->last_query(); DIE();
        	$query = $this->db->get();
        }
        //echo $this->db->last_query();
        return $query->result();
    }

    public function GetProductsDetailsTotal() {
        $IDs = $this->input->post('sub_products');
        if (!empty($IDs))
            $this->db->where_in('sub_product_id', $IDs);
        $this->db->select('SUM(amount) as total_amount,SUM(loyality_point) as total_points');
        $this->db->from($this->sub_product_table . ' sub');
        $query = $this->db->get();
        //echo $this->db->last_query();
        //print_r($query->row());
        //die;
        return $query->row();
    }

}

?>