<?php 
$class= $this->router->fetch_class();
$method=$this->router->fetch_method();
$roleIDNav = @$this->session->userdata('role');
?>
<?php if($class=='Activity' && $method=='index') { ?>
<?php $this->load->view('includes/header_calender'); ?>
<?php } else{ ?>
<?php 
$this->load->view('includes/header');
 
?>
<?php } ?>
<?php
$roleArrayNav = array('1' => '_head', '2' => '_rrm', '3' => '', '4' => '_hunter','5'=>'_rrm_leader');
?>
<?php $this->load->view('includes/navigation' . $roleArrayNav[$roleIDNav]); ?>
<div id="page-wrapper" class="gray-bg dashbard-1">
    <?php 
	
	if($roleIDNav=='4'){
		$this->load->view('includes/header_panel_hunter');
	}else if($roleIDNav=='5'){
		$this->load->view('includes/header_panel_rroleader');
	}else{
		$this->load->view('includes/header_panel'); 
	}
	?>
    <?php $this->load->view($main_content); ?>
</div>
<?php //$this->load->view('includes/chat'); ?>
<div id="right-sidebar">
    <?php //$this->load->view('includes/right_sidebar'); ?>
</div>
<?php $this->load->view('includes/footer'); ?>